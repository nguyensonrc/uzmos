import React from "react"
import { View, ViewStyle, TextStyle } from "react-native"
import { HeaderProps } from "./header.props"
import { Button } from "../button/button"
import Text from "@components/text"
import { Icon } from "../icon/icon"
import { spacing } from "../../theme"
import { translate } from "../../i18n/"
import { goBack, navigationRef } from "@navigators/navigation-utilities"
import { SafeAreaView } from "react-native-safe-area-context"

// static styles
const ROOT: ViewStyle = {
  flexDirection: "row",
  paddingHorizontal: spacing[4],
  paddingBottom: spacing[4],
  alignItems: "center",
  justifyContent: "flex-start",
}
const TITLE: TextStyle = { textAlign: "center", fontSize: 18, fontWeight: "bold" }
const TITLE_MIDDLE: ViewStyle = { flex: 1, justifyContent: "center" }
const LEFT: ViewStyle = { width: 32 }
const RIGHT: ViewStyle = { width: 32 }

/**
 * Header that appears on many screens. Will hold navigation buttons and screen title.
 */
export function Header(props: HeaderProps) {
  const {
    onLeftPress,
    onRightPress,
    rightIcon,
    leftIcon,
    headerText,
    headerTx,
    style,
    titleStyle,
  } = props
  const header =
    headerTx && headerTx.length > 0
      ? translate(headerTx)
      : (headerText && headerText.length) > 0
      ? headerText
      : headerText

  const goBackAction = () => {
    if (navigationRef.canGoBack()) {
      goBack()
    }
  }
  return (
    <View style={[ROOT, style]}>
      {leftIcon ? (
        <Button
          style={LEFT}
          preset="link"
          onPress={leftIcon === "back" ? goBackAction : onLeftPress}
        >
          <Icon icon={leftIcon} />
        </Button>
      ) : (
        <View style={LEFT} />
      )}
      <View style={TITLE_MIDDLE}>
        <Text style={[TITLE, titleStyle]} text={header} />
      </View>
      {rightIcon ? (
        <Button style={RIGHT} preset="link" onPress={onRightPress}>
          <Icon icon={rightIcon} />
        </Button>
      ) : (
        <View style={RIGHT} />
      )}
    </View>
  )
}
