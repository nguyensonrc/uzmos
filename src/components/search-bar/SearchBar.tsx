/* eslint-disable react/display-name */
import React, { forwardRef, Ref, useImperativeHandle, useState } from "react"
import { TextInput, View, Keyboard, Button } from "react-native"
import { Feather } from "@expo/vector-icons"

import styles from "./styles"

export interface RefSearch {
  searchPhrase: string
}

const SearchBar = forwardRef((_props: { searchAction; cancelAction }, ref: Ref<RefSearch>) => {
  const [searchPhrase, setSearchPhrase] = useState("")
  const [clicked, setClicked] = useState(false)

  useImperativeHandle(ref, () => ({
    searchPhrase,
  }))

  return (
    <View style={styles.container}>
      <View style={!clicked ? styles.searchBar__unclicked : styles.searchBar__clicked}>
        <Feather name="search" size={20} color="black" style={styles.feather} />
        <TextInput
          style={styles.input}
          placeholder="Search"
          value={searchPhrase}
          onChangeText={setSearchPhrase}
          onFocus={() => {
            setClicked(true)
          }}
          returnKeyType={"done"}
          onSubmitEditing={(event) => _props.searchAction()}
        />

        {/* {clicked && (
                    <Entypo name="cross" size={20} color="black" style={styles.entypo} onPress={() => {
                        setSearchPhrase("")
                    }} />
                )} */}
      </View>
      {clicked && (
        <View>
          <Button
            title="Cancel"
            onPress={() => {
              Keyboard.dismiss()
              _props.cancelAction()
              setSearchPhrase("")
              setClicked(false)
            }}
          ></Button>
        </View>
      )}
    </View>
  )
})

export default SearchBar
