import { StyleSheet } from "react-native"

const COLOR = "#d9dbda"

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "flex-start",
    marginHorizontal: 15,
    width: "90%",
  },
  entypo: {
    padding: 1,
  },
  feather: {
    marginLeft: 1,
  },
  input: {
    fontSize: 14,
    marginLeft: 10,
    width: "90%",
  },
  searchBar__clicked: {
    alignItems: "center",
    backgroundColor: COLOR,
    borderRadius: 15,
    flexDirection: "row",
    justifyContent: "space-evenly",
    padding: 10,
    width: "80%",
  },
  searchBar__unclicked: {
    alignItems: "center",
    backgroundColor: COLOR,
    borderRadius: 15,
    flexDirection: "row",
    padding: 10,
    width: "100%",
  },
})

export default styles
