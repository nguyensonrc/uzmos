import * as React from "react"
import { ViewStyle, TouchableOpacity } from "react-native"
import Ionicons from "react-native-vector-icons/Ionicons"
import AntDesign from "react-native-vector-icons/AntDesign"

export interface VectorIconProps {
  iconSet?: "ion" | "ant" | undefined
  size?: number
  name?: string
  style?: ViewStyle
  onPress?: () => void
  color?: string
  id?: string
}

const defaultSize = 18

const VectorIcon = (props: VectorIconProps) => {
  const { iconSet, onPress, name, size, style: customStyle, id, ...rest } = props
  const renderIcon = React.useCallback(() => {
    switch (iconSet) {
      case "ion":
        return <Ionicons {...rest} name={name} size={size || defaultSize} />
      case "ant":
        return <AntDesign {...rest} name={name} size={size || defaultSize} />
    }
  }, [iconSet, name, rest])

  return (
    <TouchableOpacity
      key={id}
      style={customStyle}
      onPress={onPress}
      hitSlop={{ top: 10, right: 10, left: 10, bottom: 10 }}
      disabled={!onPress}
    >
      {renderIcon()}
    </TouchableOpacity>
  )
}

export default VectorIcon
