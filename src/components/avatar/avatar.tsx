import VectorIcon from "@components/vectorIcon/vectorIcon"
import { color } from "@theme/color"
import { Avatar as NativeAvatar, Box, IAvatarProps } from "native-base"
import * as React from "react"
import { ActivityIndicator, ImageSourcePropType, TouchableOpacity, View } from "react-native"

import { styles } from "./styles"

interface AvatarProps extends IAvatarProps {
  source?: ImageSourcePropType
  isLoading?: boolean
  disabled?: boolean
  errorMsg?: string
  onPress?: () => void
}

export const Avatar = (props: AvatarProps) => {
  const { onPress, source, isLoading, errorMsg, rounded = "full", ...rest } = props

  const errorStyle = errorMsg ? { borderWidth: 1.5, borderColor: color.palette.angry } : null
  const url = source as any

  const isImage = url && url.uri && url.uri !== undefined && url.uri !== null

  return (
    <TouchableOpacity
      {...rest}
      onPress={onPress}
      hitSlop={{ bottom: 10, top: 10, left: 10, right: 10 }}
    >
      {isImage ? (
        <NativeAvatar
          source={source}
          size={"2xl"}
          alignSelf="center"
          rounded={rounded}
          _image={{
            rounded,
          }}
          {...errorStyle}
        />
      ) : (
        <View style={styles.avatar}>
          <VectorIcon iconSet="ion" name="images" size={26} />
        </View>
      )}
      {isLoading && (
        <Box
          backgroundColor={color.alpha.black50}
          // flex={1}
          rounded={rounded}
          w="32"
          position="absolute"
          justifyContent={"center"}
          alignSelf={"center"}
          top={0}
          bottom={0}
        >
          <ActivityIndicator />
        </Box>
      )}
      {errorMsg && (
        <Box
          position={"absolute"}
          top={"75%"}
          left={"58%"}
          backgroundColor={color.palette.white}
          borderRadius={24}
        >
          <VectorIcon
            iconSet="ant"
            name="exclamationcircleo"
            size={26}
            color={color.palette.angry}
          />
        </Box>
      )}
    </TouchableOpacity>
  )
}
