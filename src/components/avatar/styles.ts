import { color } from "@theme/color"
import { StyleSheet } from "react-native"

export const styles = StyleSheet.create({
  avatar: {
    alignItems: "center",
    alignSelf: "center",
    borderColor: color.palette.lighterGrey,
    borderRadius: 45,
    borderWidth: 1,
    height: 90,
    justifyContent: "center",
    padding: 3,
    width: 90,
  },
})
