import React, { useEffect } from "react"
import { Box } from "native-base"
import { openCamera, openPicker, Image } from "react-native-image-crop-picker"

import { ButtonCustom } from "@components/button/buttonCustom"
import { color } from "@theme/color"
import { spacing } from "@theme/spacing"
import { useUtility } from "@hooks/utility"
import { styles } from "./styles"

interface IImagePicker {
  onImageSelect: (data: Image) => void
}

const options = [
  { id: "camera", onPress: openCamera },
  { id: "imageLibrary", onPress: openPicker },
]

const ImagePicker: React.FC<IImagePicker> = ({ onImageSelect }) => {
  // const [selectedImageUrl, setSelectedImageUrl] = useState<string>()

  const handleOptionPress = async (callback: Promise<Image>) => {
    const res = await callback
    onImageSelect(res)
  }

  return (
    <Box
      backgroundColor={color.palette.white}
      borderTopLeftRadius={"3xl"}
      borderTopRightRadius={"3xl"}
    >
      {options.map((item, index) => (
        <ButtonCustom
          key={item.id}
          text={item.id}
          onPress={() => handleOptionPress(item.onPress({ mediaType: "photo", cropping: true }))}
          w="full"
          paddingTop={spacing[1]}
          paddingBottom={spacing[1]}
          backgroundColor={color.palette.white}
          textStyle={styles.optionText}
        />
      ))}
    </Box>
  )
}

export default ImagePicker
