/* eslint-disable react/display-name */
import React, { forwardRef, ReactNode, Ref, useImperativeHandle, useState } from "react"
import { ViewStyle } from "react-native"
import ReactNativeModal from "react-native-modal"
import { styles } from "./styles"

interface ICustomModal {
  childView: ReactNode
  style?: ViewStyle
  lockBackdropPress?: boolean
}

export interface IRefCustomModal {
  openModal: () => void
  closeModal: () => void
}

const CustomModal = forwardRef((props: ICustomModal, ref: Ref<IRefCustomModal>) => {
  const [visible, setVisible] = useState(false)

  useImperativeHandle(ref, () => ({
    openModal,
    closeModal,
  }))

  const openModal = () => {
    if (!visible) {
      setVisible(true)
    }
  }

  const closeModal = () => {
    if (visible) {
      setVisible(false)
    }
  }

  return (
    <ReactNativeModal
      isVisible={visible}
      onBackdropPress={props.lockBackdropPress ? null : closeModal}
      style={[styles.modal, props.style]}
    >
      {props.childView}
    </ReactNativeModal>
  )
})

export default CustomModal
