import * as React from "react"
import { DATE_FORMAT, WIDTH_WINDOW } from "@config/constants"
import { Calendar, CalendarList, Agenda, DateData } from "react-native-calendars"

import styles from "./styles"
import { FC } from "react"

interface IViewPros {
  currentDate: moment.Moment
  selectedDate: (date: DateData) => void
}

export const CustomCalendarList: FC<IViewPros> = ({ currentDate, selectedDate }) => {
  return (
    <CalendarList
      style={styles.calendarList}
      // Enable horizontal scrolling, default = false
      horizontal={true}
      // Enable paging on horizontal, default = false
      pagingEnabled={true}
      // Set custom calendarWidth.
      calendarWidth={WIDTH_WINDOW}
      current={currentDate.format(DATE_FORMAT).toString()}
      markedDates={{
        [currentDate.format(DATE_FORMAT).toString()]: {
          selected: true,
          marked: true,
          selectedColor: "green",
        },
      }}
      onDayPress={selectedDate}
    />
  )
}
