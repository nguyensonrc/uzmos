import { LoginEmail, SignUpEmail, UpdateCompany } from "@models/backend/request/Auth"
import { RegisterDTO, SignUpEmailDTO } from "@models/backend/response/Auth"
import { useStores } from "@models/root-store"
import { FirebaseAuthTypes } from "@react-native-firebase/auth"
import {
  continueWithAppleApi,
  continueWithFacebookApi,
  continueWithGoogleApi,
  forgotPasswordApi,
  loginApi,
  registerApi,
  signUpEmailApi,
  updateCompanyApi,
} from "@services/api/Auth"
import { useState } from "react"

type Output = {
  loading: boolean
  userProfile: SignUpEmailDTO
  registerErrorCode: null | number
  forgotPasswordStatus: null | boolean
  registerStatus: boolean
  socialContinueStatusCode: null | number
  signUpEmailInvoke: (params: SignUpEmail) => void
  register: (params: SignUpEmail) => void
  login: (params: LoginEmail) => void
  forgotPassword: (params: string) => void
  continueWithSocial: (
    name: "google" | "apple" | "facebook",
    user: FirebaseAuthTypes.User,
    token: string,
  ) => void
  // continueWithGoogle: (data: FirebaseAuthTypes.User, token: string) => void
  // continueWithFacebook: (data: FirebaseAuthTypes.User, token: string) => void
  // continueWithApple: (data: FirebaseAuthTypes.User, token: string) => void
  updateCompany: (data: UpdateCompany, id: number, token: string) => void
}

export const useAuth = (): Output => {
  const [loading, setLoading] = useState<boolean>(false)
  const [userProfile, setUserProfile] = useState<SignUpEmailDTO>(null)
  const [registerStatus, setRegisterStatus] = useState<boolean>(false)
  const [registerErrorCode, setRegisterErrorCode] = useState<null | number>(null)
  const [socialContinueStatusCode, setSocialContinueStatusCode] = useState<null | number>(null)
  const [forgotPasswordStatus, setForgotPasswordStatus] = useState<null | boolean>(null)
  const { userStore, authStore } = useStores()
  const { saveUser, saveUserId } = userStore
  const { saveAuth } = authStore

  const signUpEmailInvoke = async (params: SignUpEmail) => {
    try {
      setLoading(true)
      const response = await signUpEmailApi(params)
      setUserProfile(response.data)
    } catch (err) {
      setLoading(false)
    } finally {
      setLoading(false)
    }
  }

  const register = async (params: SignUpEmail) => {
    try {
      setLoading(true)
      const { data } = await registerApi(params)
      const { accessToken, userInfo } = data
      setRegisterStatus(true)
      setLoading(false)
      setTimeout(() => {
        saveAuth(accessToken.token, accessToken.refreshToken)
        saveUser(userInfo)
      }, 200)
    } catch (err) {
      setRegisterErrorCode(err.statusCode)
    }
  }

  const login = async (params: LoginEmail) => {
    try {
      setLoading(true)
      const response = await loginApi(params)
      const { accessToken, id } = response.data
      saveUserId(id)
      saveAuth(accessToken.token, accessToken.refreshToken)
      setLoading(false)
    } catch (err) {
      console.log("error", err)
    }
    setLoading(false)
  }

  const forgotPassword = async (email: string) => {
    try {
      setLoading(true)
      const response = await forgotPasswordApi(email)
      setForgotPasswordStatus(response.data.status)
      setLoading(false)
    } catch (err) {
      console.log("error", err)
    }
  }

  const continueWithSocial = async (
    name: "google" | "apple" | "facebook",
    user: FirebaseAuthTypes.User,
    token: string,
  ) => {
    try {
      setLoading(true)
      let response: { data: RegisterDTO; status: number }
      switch (name) {
        case "apple":
          response = await continueWithAppleApi(user, token)
          break
        case "facebook":
          response = await continueWithFacebookApi(user, token)
          break
        case "google":
          response = await continueWithGoogleApi(user, token)
          break
      }
      const { data, status } = response
      const { userInfo, accessToken } = data
      saveUser(userInfo)
      saveAuth(accessToken.token, accessToken.refreshToken)
      setSocialContinueStatusCode(status)
      setLoading(false)
    } catch (err) {
      setSocialContinueStatusCode(err.statusCode)
    }
  }

  const updateCompany = async (data: UpdateCompany, id: number, token: string) => {
    try {
      setLoading(true)
      await updateCompanyApi(data, id, token)
      setRegisterStatus(true)
      setLoading(false)
    } catch (err) {
      setRegisterErrorCode(err.statusCode)
    }
  }

  return {
    loading,
    userProfile,
    registerErrorCode,
    forgotPasswordStatus,
    registerStatus,
    socialContinueStatusCode,
    signUpEmailInvoke,
    register,
    login,
    forgotPassword,
    continueWithSocial,
    // continueWithGoogle,
    // continueWithFacebook,
    // continueWithApple,
    updateCompany,
  }
}
