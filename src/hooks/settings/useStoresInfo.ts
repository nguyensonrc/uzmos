import { UpdateStore } from "@models/backend/request/Store"
import { StoreDTO } from "@models/backend/response/Store"
import { getStoresApi, updateStoreApi } from "@services/api/Store"
import { useState } from "react"

type Output = {
  // list store
  loadingStores: boolean
  stores: StoreDTO[]
  errStores: Error
  errUpdateStore: Error
  updating: boolean
  updateSuccess: boolean
  getStores: () => void
  updateStore: (id: number, data: UpdateStore) => void
  // update company hour
  loadingCompanyHour: boolean
  companyHour: any
  errCompanyHour: Error
  updateCompanyHour: (id: number, data: UpdateStore) => void
}

export const useStoresInfo = (): Output => {
  // list store
  const [loadingStores, setLoadingStores] = useState<boolean>(false)
  const [stores, setStores] = useState<StoreDTO[]>([])
  const [errStores, setErrStores] = useState<Error>(null)
  // update company hour
  const [loadingCompanyHour, setLoadingCompanyHour] = useState<boolean>(false)
  const [companyHour, setCompanyHour] = useState<any>()
  const [errCompanyHour, setErrCompanyHour] = useState<Error>(null)

  const updateCompanyHour = async (id: number, data: UpdateStore) => {
    try {
      setLoadingCompanyHour(true)
      const result = await updateStoreApi(id, data)
      if (result?.data) {
        setLoadingCompanyHour(false)
        setCompanyHour(result?.data)
        setErrCompanyHour(null)
      }else{
        const newErr = new Error("Data is empty!")
        setLoadingCompanyHour(false)
        setCompanyHour(null)
        setErrCompanyHour(newErr)
      }
    } catch (err) {
      setLoadingCompanyHour(false)
      setErrCompanyHour(err)
    }
  }



  const getStores = async () => {
    try {
      setLoadingStores(true)
      const result = await getStoresApi()
      if (result && result.data && result.data.length > 0) {
        setLoadingStores(false)
        setStores(result.data)
        setErrStores(null)
      } else {
        const newErr = new Error("Data is empty!")
        setLoadingStores(false)
        setStores([])
        setErrStores(newErr)
      }
    } catch (err) {
      setErrStores(err)
      setLoadingStores(false)
    }
  }

  // update store
  const [updating, setUpdating] = useState(false)
  const [updateSuccess, setUpdateSuccess] = useState(false)
  const [errUpdateStore, setErrUpdateStore] = useState<Error>(null)

  const updateStore = async (id: number, data: UpdateStore) => {
    try {
      setUpdating(true)
      const result = await updateStoreApi(id, data)
      if (result?.data && result.data.affected === 1) {
        setUpdateSuccess(true)
        setErrUpdateStore(null)
        setUpdating(false)
      } else {
        const newErr = new Error("Store data upadte not successful")
        setErrUpdateStore(newErr)
        setUpdating(false)
      }
    } catch (err) {
      setErrUpdateStore(err)
      setUpdating(false)
    }
  }

  return {
    loadingStores,
    stores,
    errStores,
    updating,
    updateSuccess,
    errUpdateStore,
    getStores,
    updateStore,
    loadingCompanyHour,
    companyHour,
    errCompanyHour,
    updateCompanyHour
  }
}
