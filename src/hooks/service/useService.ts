import {
  EditCategory, EditListServiceInCategory, ListSearch, ListServiceInCategory, NewCategory, NewService
} from "@models/backend/request/Service"
import { CategoryDTO, ServiceDTO, ServiceInCategoryDTO } from "@models/backend/response/Service"
import {
  addNewCategoryApi,
  addNewServiceApi,
  editCategoryApi,
  getCatListApi,
  getListServiceInCategoryAPI,
  getServiceListApi,
  updateListServiceInCategoryAPI
} from "@services/api/Service"
import { useState } from "react"

type Output = {
  // list service
  loadingServiceList: boolean
  serviceList: ServiceDTO[]
  errServiceList: Error
  getServiceList: (search: string) => void
  // new service
  loadingNewService: boolean
  newService: ServiceDTO
  errNewService: Error
  addNewService: (data: Partial<NewService>) => void
  // cat
  loadingCatList: boolean
  catList: CategoryDTO[]
  errCatList: Error
  getCatList: (search: string) => void
  // new cat
  loadingNewCategory: boolean
  newCategory: CategoryDTO
  errNewCategory: Error
  addNewCategory: (data: Partial<NewCategory>) => void
  editCategory: (data: Partial<EditCategory>) => void
  editCategoryStatus: boolean
  // service in categort
  getListServiceInCategory: (id: number) => void
  serviceInCategory: ServiceInCategoryDTO[]
  setNewListServerInCategory: (data: Partial<ServiceInCategoryDTO[]>) => void
  updateListServerInCategory: (data: Partial<ServiceInCategoryDTO[]>, id: number) => void
}

export const useService = (): Output => {
  // list service
  const [loadingServiceList, setLoadingServiceList] = useState<boolean>(false)
  const [serviceList, setServiceList] = useState<ServiceDTO[]>([])
  const [errServiceList, setErrServiceList] = useState<Error>(null)
  // new service
  const [loadingNewService, setLoadingNewService] = useState<boolean>(false)
  const [newService, setNewService] = useState<ServiceDTO>(null)
  const [errNewService, setNewErrService] = useState<Error>(null)
  // cat
  const [loadingCatList, setLoadingCatList] = useState<boolean>(false)
  const [catList, setCatList] = useState<CategoryDTO[]>([])
  const [errCatList, setErrCatList] = useState<Error>(null)
  // new cat
  const [loadingNewCategory, setLoadingNewCategory] = useState<boolean>(false)
  const [newCategory, setNewCategory] = useState<CategoryDTO>(null)
  const [errNewCategory, setNewErrCategory] = useState<Error>(null)
  const [editCategoryStatus, setEditCategoryStatus] = useState<boolean>(false)
  // service in category
  const [serviceInCategory, setServiceInCategory] = useState<ServiceInCategoryDTO[]>([])

  const getServiceList = async (data: string) => {
    try {
      const query = { search: data } as ListSearch
      setLoadingServiceList(true)
      const result = await getServiceListApi(query)
      if (result && result.data && result.data.length > 0) {
        // console.log("1111")
        setLoadingServiceList(false)
        setServiceList(result.data)
        setErrServiceList(null)
      } else {
        // console.log(result)
        const newErr = new Error("Data is empty!")
        setLoadingServiceList(false)
        setServiceList([])
        setErrServiceList(newErr)
      }
    } catch (err) {
      // console.log(err)
      setErrServiceList(err)
      setLoadingServiceList(false)
    }
  }

  // new service
  const addNewService = async (data: NewService) => {
    try {
      setLoadingNewService(true)
      const result = await addNewServiceApi(data)
      if (result && result.data) {
        // console.log("1111")
        setLoadingNewService(false)
        setNewService(result.data)
        setNewErrService(null)
      } else {
        // console.log(result)
        const newErr = new Error("Data is empty!")
        setLoadingNewService(false)
        setNewService(null)
        setNewErrService(newErr)
      }
    } catch (err) {
      // console.log(err)
      setNewErrService(err)
      setLoadingNewService(false)
    }
  }

  // cat
  const getCatList = async (data: string) => {
    try {
      const query = { search: data } as ListSearch
      setLoadingCatList(true)
      const result = await getCatListApi(query)
      if (result && result.data && result.data.length > 0) {
        setLoadingCatList(false)
        setCatList(result.data)
        setErrCatList(null)
      } else {
        const newErr = new Error("Data is empty!")
        setLoadingCatList(false)
        setCatList([])
        setErrCatList(newErr)
      }
    } catch (err) {
      // console.log("getCatList 2 " + err)
      setCatList([])
      setErrCatList(err)
    }
  }

  // new service
  const addNewCategory = async (data: NewCategory) => {
    try {
      setLoadingNewCategory(true)
      const result = await addNewCategoryApi(data)
      if (result && result.data) {
        // console.log("1111")
        setLoadingNewCategory(false)
        setNewCategory(result.data)
        setNewErrService(null)
      } else {
        // console.log(result)
        const newErr = new Error("Data is empty!")
        setLoadingNewCategory(false)
        setNewCategory(null)
        setNewErrCategory(newErr)
      }
    } catch (err) {
      // console.log(err)
      setNewErrCategory(err)
      setLoadingNewCategory(false)
    }
  }

  // edit category
  const editCategory = async (data: EditCategory) => {
    try {
      setLoadingNewCategory(true)
      const result = await editCategoryApi(data)
      if (result && result.data) {
        // console.log("1111")
        setLoadingNewCategory(false)
        setNewCategory(result.data)
        setNewErrService(null)
        setEditCategoryStatus(true)
      } else {
        // console.log(result)
        const newErr = new Error("Data is empty!")
        setLoadingNewCategory(false)
        setNewCategory(null)
        setNewErrCategory(newErr)
        setEditCategoryStatus(false)
      }
    } catch (err) {
      // console.log(err)
      setNewErrCategory(err)
      setLoadingNewCategory(false)
      setEditCategoryStatus(false)
    }
  }

  // list service in category
  const getListServiceInCategory = async (id: number) => {
    try {
      const query = { id } as ListServiceInCategory
      setLoadingCatList(true)
      const result = await getListServiceInCategoryAPI(query)
      if (result && result.data && result.data.length > 0) {
        setLoadingCatList(false)
        setServiceInCategory(result.data)
        setErrCatList(null)
      } else {
        // console.log("getCatList 1 " + result)
        const newErr = new Error("Data is empty!")
        setLoadingCatList(false)
        setServiceInCategory([])
        setErrCatList(newErr)
      }
    } catch (err) {
      // console.log("getCatList 2 " + err)
      setServiceInCategory([])
      setErrCatList(err)
    }
  }

  // update list service in catagory
  const setNewListServerInCategory = async (_serviceInCategory: ServiceInCategoryDTO[]) => {
    try {
      setServiceInCategory(_serviceInCategory)
      setErrCatList(null)
    } catch (err) {
      setServiceInCategory([])
      setErrCatList(err)
    }
  }

  // update list service in catagory
  const updateListServerInCategory = async (data: ServiceInCategoryDTO[], id: number) => {
    try {
      const value = 3

      let arr = [1, 2, 3, 4, 5, 3]
      console.log(arr);
      arr = arr.filter(item => item !== value)
      console.log(arr);

      data = data.filter(item => item.selected === 1 && item.addNew === 0)
      const query = { categories: data } as EditListServiceInCategory
      setLoadingNewCategory(true)
      console.log({data})
      const result = await updateListServiceInCategoryAPI(query, id)
      if (result && result.data) {
        // console.log("1111")
        setLoadingNewCategory(false)
        setNewErrService(null)
        setEditCategoryStatus(true)
      } else {
        // console.log("2222", result)
        const newErr = new Error("Data is empty!")
        setLoadingNewCategory(false)
        setNewCategory(null)
        setNewErrCategory(newErr)
        setEditCategoryStatus(false)
      }
    } catch (err) {
      // console.log("3333 ", err._response)
      setNewErrCategory(err)
      setLoadingNewCategory(false)
      setEditCategoryStatus(false)
    }
  }

  return {
    loadingServiceList,
    serviceList,
    errServiceList,
    getServiceList,
    getCatList,
    catList,
    errCatList,
    loadingNewService,
    loadingCatList,
    errNewService,
    newService,
    addNewService,
    loadingNewCategory,
    newCategory,
    errNewCategory,
    addNewCategory,
    editCategory,
    editCategoryStatus,
    getListServiceInCategory,
    serviceInCategory,
    setNewListServerInCategory,
    updateListServerInCategory,
  }
}
