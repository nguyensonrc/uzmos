import { UploadDTO } from "@models/backend/response/Ulities"
import { uploadApi } from "@services/api/Utility"
import { useEffect, useState } from "react"
import { Image } from "react-native-image-crop-picker"

interface Output {
  loading: boolean
  imageData: UploadDTO
  uploadingImage: (photo: Image) => void
}

export const useUtility = (): Output => {
  const [loading, setLoading] = useState(false)
  const [imageData, setImageData] = useState<UploadDTO>()

  // useEffect(() => {
  //   setImageData(undefined)
  //   return () => {
  //     setImageData((prev) => prev)
  //   }
  // }, [])

  const uploadingImage = async (photo: Image) => {
    try {
      setLoading(true)
      const formData = new FormData()
      formData.append("file", {
        uri: photo.path,
        type: photo.mime,
        name: photo.path.substring(photo.path.lastIndexOf("/" + 1)),
      } as unknown as Blob)
      const res = await uploadApi(formData)
      setImageData(res.data)
      setLoading(false)
    } catch (err) {
      setLoading(false)
    }
  }

  return { loading, imageData, uploadingImage }
}
