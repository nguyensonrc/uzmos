import { Staff } from "@models/backend/request/Staff"
import { StaffDTO } from "@models/backend/response/Staff"
import {
  createStaffProfileApi,
  deleteStaffProfileApi,
  editStaffProfileApi,
  getStaffApi,
} from "@services/api/Staff"
import { useState } from "react"

interface Output {
  loading: boolean
  loadingDelete: boolean
  editStaffStatus: boolean
  createStaffStatus: boolean
  deleteStaffStatus: boolean
  staffs: StaffDTO[]
  getStaff: () => void
  editStaffProfile: (data: StaffDTO) => void
  createStaffProfile: (data: Staff) => void
  deleteStaffProfile: (id: number) => void
}

export const useStaff = (): Output => {
  const [loading, setLoading] = useState(false)
  const [loadingDelete, setLoadingDelete] = useState(false)
  const [staffs, setStaffs] = useState<Partial<Array<StaffDTO>>>()
  const [editStaffStatus, setEditStaffStatus] = useState(false)
  const [createStaffStatus, setCreateStaffStatus] = useState(false)
  const [deleteStaffStatus, setDeleteStaffStatus] = useState(false)

  const getStaff = async () => {
    try {
      setLoading(true)
      const { data } = await getStaffApi()
      setStaffs(data)
      setLoading(false)
    } catch (err) {
      console.log("get staff error: ", err)
      setLoading(false)
    }
  }

  const editStaffProfile = async (profile: StaffDTO) => {
    try {
      setLoading(true)
      const { data } = await editStaffProfileApi(profile)
      if (data?.id) {
        setEditStaffStatus(true)
      } else {
        setLoading(false)
        setEditStaffStatus(false)
      }
    } catch (err) {
      setLoading(false)
      setEditStaffStatus(false)
    }
  }

  const createStaffProfile = async (profile: Staff) => {
    try {
      setLoading(true)
      const { data } = await createStaffProfileApi(profile)
      if (data?.id) {
        setCreateStaffStatus(true)
      }
      setLoading(false)
    } catch (err) {
      setLoading(false)
    }
  }

  const deleteStaffProfile = async (id: number) => {
    try {
      setLoadingDelete(true)
      const { data } = await deleteStaffProfileApi(id)
      if (data?.id) {
        setDeleteStaffStatus(true)
      }
      setLoadingDelete(false)
    } catch (err) {
      setLoadingDelete(false)
    }
  }

  return {
    loading,
    loadingDelete,
    staffs,
    editStaffStatus,
    createStaffStatus,
    deleteStaffStatus,
    getStaff,
    editStaffProfile,
    createStaffProfile,
    deleteStaffProfile,
  }
}
