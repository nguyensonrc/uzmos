import { CreateAppointment } from "@models/backend/request/Appointment"
import { AppointmentDTO, CalendarDTO } from "@models/backend/response/Appointment"
import { addNewAppointmentApi, listAppointmentApi } from "@services/api/Appontment"
import moment from "moment"
import { useState } from "react"

type Output = {
  // new appointment
  loadingNewAppointment: boolean
  newAppointmnet: AppointmentDTO
  errNewAppoint: Error
  addNewAppointment: (data: Partial<CreateAppointment>) => void
  // list appointment
  loadingAppointment: boolean
  listAppointmnet: AppointmentDTO[]
  errListAppointment: Error
  getListAppointment: () => void
  listAppointmnetTemp: CalendarDTO[]
}

export const useAppointment = (): Output => {
  // new appointment
  const [loadingNewAppointment, setLoadingNewAppointment] = useState<boolean>(false)
  const [newAppointmnet, setNewAppointmnet] = useState<AppointmentDTO>(null)
  const [errNewAppoint, setErrNewAppoint] = useState<Error>(null)

  // list appointment
  const [loadingAppointment, setLoadingAppointment] = useState<boolean>(false)
  const [listAppointmnet, setListAppointmnet] = useState<AppointmentDTO[]>([])
  const [listAppointmnetTemp, setListAppointmnetTemp] = useState<CalendarDTO[]>([])
  const [errListAppointment, setErrListAppointment] = useState<Error>(null)

  // new appointment
  const addNewAppointment = async (data: CreateAppointment) => {
    try {
      setLoadingNewAppointment(true)
      const result = await addNewAppointmentApi(data)
      if (result && result.data) {
        console.log(result.data)
        setLoadingNewAppointment(false)
        setNewAppointmnet(result.data)
        setErrNewAppoint(null)
      } else {
        // console.log("result")
        const newErr = new Error("Cannot create appointment!")
        setLoadingNewAppointment(false)
        setNewAppointmnet(null)
        setErrNewAppoint(newErr)
      }
    } catch (err) {
      const newErr = new Error("Cannot create appointment!")
      setErrNewAppoint(newErr)
      setLoadingNewAppointment(false)
    }
  }

  // list appointment
  const getListAppointment = async () => {
    try {
      setLoadingAppointment(true)
      const result = await listAppointmentApi()
      if (result && result.data && result.data.length > 0) {
        // const arr = [result.data.pop()] as AppointmentDTO[]
        const arrCalendar = [] as CalendarDTO[]
        result.data.forEach((element) => {
          if (
            element.date &&
            element.service &&
            element.duration &&
            moment(element.date).isValid()
          ) {
            const startTemp = moment(element.date).local()
            const min = startTemp.minutes()
            const item = {
              start: startTemp.format("YYYY-MM-DD HH:mm:ss"),
              end: startTemp.set({ m: min + element.duration }).format("YYYY-MM-DD HH:mm:ss"),
              title: element.service && element.service.name ? element.service.name : "",
              summary: element.staff && element.staff.name ? element.staff.name : "",
            }
            // console.log({ item })
            arrCalendar.push(item)
          }
        })
        // console.log("list", arrCalendar)
        setLoadingAppointment(false)
        setListAppointmnetTemp(arrCalendar)
        // setListAppointmnet(result.data)
        setErrListAppointment(null)
      } else {
        // console.log(result)
        const newErr = new Error("Data is empty!")
        setLoadingAppointment(false)
        setListAppointmnetTemp([])
        // setListAppointmnet([])
        setErrListAppointment(newErr)
      }
    } catch (err) {
      // console.log(err)
      setErrListAppointment(err)
      setLoadingAppointment(false)
    }
  }

  return {
    loadingNewAppointment,
    newAppointmnet,
    errNewAppoint,
    addNewAppointment,
    loadingAppointment,
    listAppointmnet,
    errListAppointment,
    getListAppointment,
    listAppointmnetTemp,
  }
}
