import { NewProduct } from "@models/backend/request/Product"
import { ProductDTO } from "@models/backend/response/Product"
import { createProductApi, getProductsApi, updateProductApi } from "@services/api/Product"
import { useState } from "react"

interface Output {
  loading: boolean
  products: Array<ProductDTO>
  updateProductStatus: boolean
  createProductStatus: boolean
  getProducts: (search: string) => void
  createProduct: (data: NewProduct) => void
  updateProduct: (product: ProductDTO) => void
}

export const useProduct = (): Output => {
  const [loading, setLoading] = useState(false)
  const [updateProductStatus, setUpdateProductStatus] = useState(false)
  const [createProductStatus, setCreateProductStatus] = useState(false)
  const [products, setProducts] = useState<Array<ProductDTO>>()

  const getProducts = async (search: string) => {
    try {
      setLoading(true)
      const { data } = await getProductsApi(search)
      console.log("ProductList", data)
      setProducts(data)
      setLoading(false)
    } catch (err) {
      console.log("Get products error: ", err)
      setLoading(false)
    }
  }

  const createProduct = async (data: NewProduct) => {
    try {
      setLoading(true)
      const res = await createProductApi(data)
      if (res?.data) {
        setUpdateProductStatus(true)
      }
      setLoading(false)
    } catch (err) {
      console.log("Update product fail: ", err)
      setLoading(false)
    }
  }

  const updateProduct = async (product: ProductDTO) => {
    try {
      setLoading(true)
      const res = await updateProductApi(product)
      if (res?.data) {
        setCreateProductStatus(true)
      }
      setLoading(false)
    } catch (err) {
      console.log("Create product fail: ", err)
      setLoading(false)
    }
  }

  return {
    loading,
    products,
    updateProductStatus,
    createProductStatus,
    getProducts,
    createProduct,
    updateProduct,
  }
}
