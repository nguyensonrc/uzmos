import { Staff } from "@models/backend/request/Staff"
import { StaffDTO } from "@models/backend/response/Staff"
import api from "../axios/api-config"

const GET_STAFF = "appointment/staff"
const EDIT_STAFF = "appointment/staff"
const CREATE_STAFF = "appointment/staff"
const DELETE_STAFF = "appointment/staff"

export const getStaffApi = () => api<StaffDTO[]>(GET_STAFF, "GET")
export const editStaffProfileApi = (data: StaffDTO) => api<StaffDTO>(EDIT_STAFF, "PATCH", data)
export const createStaffProfileApi = (data: Staff) => api<StaffDTO>(CREATE_STAFF, "POST", data)
export const deleteStaffProfileApi = (id: number) =>
  api<StaffDTO>(DELETE_STAFF + "/" + id, "DELETE")
