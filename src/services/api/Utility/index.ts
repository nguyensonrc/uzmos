import { UploadDTO } from "@models/backend/response/Ulities"
import api from "../axios/api-config"

const UPLOAD = "/upload"

export const uploadApi = (data: FormData) =>
  api<UploadDTO>(UPLOAD, "POST", data, {
    "Content-Type": "multipart/form-data",
  })
