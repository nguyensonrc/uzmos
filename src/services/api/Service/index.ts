import {
  CategorySearch,
  EditCategory,
  EditListServiceInCategory,
  ListSearch,
  ListServiceInCategory,
  NewCategory,
  NewService,
} from "@models/backend/request/Service"
import { CategoryDTO, ServiceDTO, ServiceInCategoryDTO, UpdateServiceInCategoryDTO } from "@models/backend/response/Service"
import api from "../axios/api-config"

const SERVICE_LIST = "/appointment/service"
const SERVICE_ADD = "/appointment/service"
const CATEGORY_LIST = "/pop/category"
const CATEGORY_ADD = "/pop/category"
const SERVICE_LIST_IN_CATEGORY = "/pop/category"
const UPDATE_SERVICE_LIST_IN_CATEGORY = "/pop/category"

export function getServiceListApi(data: ListSearch) {
  return api<ServiceDTO[]>(SERVICE_LIST, "GET", undefined, {}, data)
}

export function addNewServiceApi(data: NewService) {
  return api<ServiceDTO>(SERVICE_ADD, "POST", data)
}

export function getCatListApi(data: CategorySearch) {
  return api<CategoryDTO[]>(CATEGORY_LIST, "GET", undefined, {}, data)
}

export function addNewCategoryApi(data: NewCategory) {
  return api<CategoryDTO>(CATEGORY_ADD, "POST", data)
}

export function editCategoryApi(data: EditCategory) {
  return api<CategoryDTO>(CATEGORY_ADD + "/" + data.id, "PUT", data)
}

export function getListServiceInCategoryAPI(data: ListServiceInCategory) {
  return api<ServiceInCategoryDTO[]>(
    SERVICE_LIST_IN_CATEGORY + "/" + data.id + "/services",
    "GET",
    undefined,
    {},
    data,
  )
}

export function updateListServiceInCategoryAPI(data: EditListServiceInCategory, id: number) {
  return api<UpdateServiceInCategoryDTO>(
    UPDATE_SERVICE_LIST_IN_CATEGORY + "/" + id + "/services",
    "PUT",
    data
  )
}
