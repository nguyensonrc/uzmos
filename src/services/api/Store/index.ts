import { StoreDTO } from "@models/backend/response/Store"
import api from "../axios/api-config"

const GET_STORES = "/store"
const UPDATE_STORE = "/store/update-store"

export const getStoresApi = () => api<StoreDTO[]>(GET_STORES, "GET")
export const updateStoreApi = (id: number, payload: StoreDTO) =>
  api<any>(`${UPDATE_STORE}/${id}`, "PUT", payload)

