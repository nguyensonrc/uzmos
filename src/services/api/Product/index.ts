import { NewProduct } from "@models/backend/request/Product"
import { ProductDTO } from "@models/backend/response/Product"
import apiConfig from "../axios/api-config"

const GET_PRODUCTS = "pop/product"
const UPDATE_PRODUCTS = "pop/product"

export const getProductsApi = (search: string) =>
  apiConfig<Array<ProductDTO>>(GET_PRODUCTS, "GET", null, {}, { search })

export const updateProductApi = (data: ProductDTO) =>
  apiConfig<ProductDTO>(UPDATE_PRODUCTS, "PUT", data)

export const createProductApi = (data: NewProduct) =>
  apiConfig<ProductDTO>(UPDATE_PRODUCTS, "POST", data)
