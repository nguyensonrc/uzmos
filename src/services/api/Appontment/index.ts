import { CreateAppointment } from "@models/backend/request/Appointment"
import { AppointmentDTO } from "@models/backend/response/Appointment"
import api from "../axios/api-config"

const NEW_APPOINTMENT = "/appointment/booking"
const APPOINTMENT_LIST = "/appointment/booking"

export function addNewAppointmentApi(data: CreateAppointment) {
  // console.log("data gui", data)
  return api<AppointmentDTO>(NEW_APPOINTMENT, "POST", data)
}

export function listAppointmentApi() {
  return api<AppointmentDTO[]>(NEW_APPOINTMENT, "GET")
}
