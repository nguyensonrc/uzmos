import React, { useEffect, useState } from "react"
import { createNativeStackNavigator } from "@react-navigation/native-stack"
import { StackNavigationProp } from "@react-navigation/stack"

import { OnboadingScreen, SignInScreen, SignUpScreen, StoreFormScreen } from "@screens/auth"
import ForgotPasswordScreen from "@screens/auth/forgotpassword"
import GoBackButton from "@components/go-back-button/goBackButton"
import { SignUpEmail } from "@models/backend/request/Auth"
import LoadingModal from "@screens/modal/LoadingModal"
import { translate } from "@i18n/translate"

import { color } from "@theme/color"
import { useStores } from "@models/root-store"
import { observer } from "mobx-react-lite"

export type NavigatorParamList = {
  onboarding: undefined
  login: undefined
  test: undefined
  signIn: undefined
  signUp: undefined
  forgotPassword: undefined
  storeForm: { data: SignUpEmail; providerName: string }
  global: undefined
  modal: undefined
}
export type authScreenProp = StackNavigationProp<NavigatorParamList>

const Stack = createNativeStackNavigator<NavigatorParamList>()

const AuthStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName="onboarding"
    >
      <Stack.Group>
        <Stack.Screen name="onboarding" component={OnboadingScreen} />
        <Stack.Screen name="signIn" component={SignInScreen} />
        <Stack.Screen
          name="signUp"
          component={SignUpScreen}
          options={{
            headerShown: true,
            headerLeft: () => <GoBackButton />,
            headerTransparent: true,
            headerTitle: "",
          }}
        />
        <Stack.Screen
          name="forgotPassword"
          component={ForgotPasswordScreen}
          options={{
            headerShown: true,
            headerLeft: () => <GoBackButton />,
            headerTransparent: true,
            headerTitle: "",
          }}
        />
        <Stack.Screen
          name="storeForm"
          component={StoreFormScreen}
          options={{
            headerShown: true,
            headerShadowVisible: false,
            headerTitleAlign: "center",
            headerTitle: translate("auth.storeForm.headerTitle"),
            headerLeft: () => <GoBackButton color={color.palette.black} />,
          }}
        />
      </Stack.Group>
      <Stack.Group>
        <Stack.Screen
          options={{
            presentation: "modal",
            headerShown: true,
            headerTitle: "",
            // headerLeft: () => <GoBackButton color={color.palette.black} />,
          }}
          name="modal"
          component={LoadingModal}
        />
      </Stack.Group>
    </Stack.Navigator>
  )
}

export default AuthStack
