import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import React from "react"

import TabIcon from "@components/tab-icon"
import { CustomerListScreen, HomeScreen, ServiceListScreen, SettingsScreen } from "@screens/index"
const Tab = createBottomTabNavigator()

function HomeTabs() {
  return (
    <Tab.Navigator
      initialRouteName={"Home"}
      screenOptions={{
        headerShown: false,
        tabBarShowLabel: false,
      }}
    >
      <Tab.Screen
        options={{
          tabBarIcon: ({ focused }) => <TabIcon isFocused={focused} tabIndex={0} />,
          tabBarTestID: "tabHome",
        }}
        name="Home"
        component={HomeScreen}
      />
      <Tab.Screen
        options={{
          tabBarIcon: ({ focused }) => <TabIcon isFocused={focused} tabIndex={1} />,
          tabBarTestID: "tabChat",
        }}
        name="Channels"
        component={ServiceListScreen}
      />
      <Tab.Screen
        options={{
          tabBarIcon: ({ focused }) => <TabIcon isFocused={focused} tabIndex={2} />,
          tabBarTestID: "tabNotification",
        }}
        name="Notification"
        component={CustomerListScreen}
      />
      <Tab.Screen
        options={{
          tabBarIcon: ({ focused }) => <TabIcon isFocused={focused} tabIndex={3} />,
          tabBarTestID: "tabProfile",
        }}
        name="Profile"
        component={SettingsScreen}
      />
    </Tab.Navigator>
  )
}

export default HomeTabs
