import { createNativeStackNavigator } from "@react-navigation/native-stack"
import { StackNavigationProp } from "@react-navigation/stack"
import React from "react"

import { CustomerDTO } from "@models/backend/response/Customer"
import { ProductDTO } from "@models/backend/response/Product"
import { CategoryDTO, ServiceDTO } from "@models/backend/response/Service"
import { StaffDTO } from "@models/backend/response/Staff"
import { StoreDTO, TimeZoneDTO } from "@models/backend/response/Store"
import {
  BreaksScreen,
  CategoryDetailScreen,
  CategoryListScreen,
  CompanyHoursScreen,
  CustomerListScreen,
  NewAppointmentScreen,
  NewCategoryScreen,
  NewServiceScreen,
  ServiceDetailScreen,
  ServiceListScreen,
  SettingsScreen,
  TimeOffDetailScreen,
  TimeOffScreen,
  TimeZonesScreen,
  WorkingDaysScreen
} from "@screens/main"
import { CustomerImportScreen, CustomerProfileScreen } from "@screens/main/customer"
import { ProductDetailScreen, ProductListScreen } from "@screens/main/product"
import { StaffListScreen } from "@screens/main/staff"
import StaffProfileScreen from "@screens/main/staff/StaffProfile"
import {
  BookingPoliciesScreen,
  BookingSlotSizeScreen,
  StoreDetailScreen
} from "@screens/main/store"
import HomeTabs from "./tabnavigator"

export type NavigatorParamList = {
  home: { hasFollowedProject: boolean } | { screen: string; params: any }
  serviceList: undefined
  categoryList: undefined
  customerList: undefined
  customerProfile: { editable?: boolean; customerProfile?: CustomerDTO }
  newService: undefined
  serviceDetail: { detail: ServiceDTO }
  newCategory: undefined
  categoryDetail: { detail: CategoryDTO, editable?: boolean }
  staffList: undefined
  staffProfile: { detail?: StaffDTO; editable?: boolean }
  categoryProfile: { detail?: CategoryDTO; editable?: boolean }
  newAppointment: undefined
  productList: undefined
  productDetail: { detail?: ProductDTO; editable?: boolean }
  workingDays: { staffDetail: StaffDTO }
  customerImport: undefined
  catagoryProfile: { detail?: CategoryDTO; editable?: boolean }
  breaks: { staffDetail: StaffDTO }
  timeOff: { staffDetail: StaffDTO }
  timeOffDetail: { staffDetail: StaffDTO }
  settings: undefined
  companyHours: { storeDetail: StoreDTO, timeZone?: TimeZoneDTO }
  timeZone: { storeDetail: StoreDTO, timeZone: TimeZoneDTO }
  storeDetail: { storeDetail: StoreDTO }
  bookingPolicies: { storeDetail: StoreDTO }
  bookingSlotSize: { storeDetail: StoreDTO }
  customerNotes: { storeDetail: StoreDTO }
  cancellationPolicy: { storeDetail: StoreDTO }
}

export type mainScreenProp = StackNavigationProp<NavigatorParamList>

const Stack = createNativeStackNavigator<NavigatorParamList>()

const MainStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName={"home"}
    >
      <Stack.Screen
        options={{
          headerShown: false,
        }}
        name="home"
        component={HomeTabs}
      />
      <Stack.Screen name="customerList" component={CustomerListScreen} />
      <Stack.Screen name="customerProfile" component={CustomerProfileScreen} />
      <Stack.Screen name="customerImport" component={CustomerImportScreen} />
      <Stack.Screen name="categoryList" component={CategoryListScreen} />
      <Stack.Screen name="newCategory" component={NewCategoryScreen} />
      <Stack.Screen name="categoryDetail" component={CategoryDetailScreen} />
      <Stack.Screen name="serviceList" component={ServiceListScreen} />
      <Stack.Screen name="newService" component={NewServiceScreen} />
      <Stack.Screen name="serviceDetail" component={ServiceDetailScreen} />
      <Stack.Screen name="newAppointment" component={NewAppointmentScreen} />
      <Stack.Screen name="staffList" component={StaffListScreen} />
      <Stack.Screen name="staffProfile" component={StaffProfileScreen} />
      <Stack.Screen name="productList" component={ProductListScreen} />
      <Stack.Screen name="productDetail" component={ProductDetailScreen} />
      <Stack.Screen name="workingDays" component={WorkingDaysScreen} />
      <Stack.Screen name="breaks" component={BreaksScreen} />
      <Stack.Screen name="timeOff" component={TimeOffScreen} />
      <Stack.Screen name="settings" component={SettingsScreen} />
      <Stack.Screen name="timeOffDetail" component={TimeOffDetailScreen} />
      <Stack.Group key={"Settings"}>
        <Stack.Screen name="storeDetail" component={StoreDetailScreen} />
        <Stack.Screen name="bookingPolicies" component={BookingPoliciesScreen} />
        <Stack.Screen name="bookingSlotSize" component={BookingSlotSizeScreen} />
      </Stack.Group>
      <Stack.Screen name="companyHours" component={CompanyHoursScreen} />
      <Stack.Screen name="timeZone" component={TimeZonesScreen} />
    </Stack.Navigator>
  )
}

export default MainStack
