export interface ProductDTO {
  id: number
  name: string
  cost: number
  price: number
  stocks: number
  description: string
  photo: string
  thumb: string
  color: string
  orderBy: number
  isActive: boolean
  isPrivate: boolean
  isService: boolean
  serviceDuration: number
  SKU: string
  storeId: number
  suppilerId: number
  categoryId: number
}

export interface ProductOption {
  id: number
  name: string
  price: number
  orderBy: number
}
