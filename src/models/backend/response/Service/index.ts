export interface ServiceDTO {
  id: number
  name: string
  cost: number
  price: number
  stocks: number
  description?: string
  photo?: string
  thumb?: string
  color?: string
  orderBy: number
  serviceDuration: number
  SKU?: string
  storeId: number
  suppilerId?: number
  categoryId?: number
}

export interface CategoryDTO {
  id: number
  name: string
  orderBy: number
  storeId: number
  isActive: boolean
  products: Array<any>
}

export interface ServiceInCategoryDTO {
  service_id: number
  category_id: number
  name: string
  selected: number
  addNew: number
}

export interface UpdateServiceInCategoryDTO {
  generatedMaps: Array<any>
  raw: Array<any>
  affected: number
}
