import { ServiceDTO } from "../Service"
import { StaffDTO } from "../Staff"

export interface AppointmentDTO {
  id: number
  customerId?: number
  serviceId?: number
  staffId?: number
  lastDate: Date
  date: Date
  status?: string
  color?: string | "#EEEEEE"
  note?: string
  storeId?: number
  isCheckIn?: boolean | false
  labelId?: number
  remiderSent?: boolean | false
  followUpSent?: boolean | false
  didNotShow: boolean | false
  didNotShowSent: boolean | false
  created: Date
  isActive?: boolean | false
  service: ServiceDTO
  staff: StaffDTO
  duration?: number
}

export interface CalendarDTO {
  start: string
  end: string
  title: string
  summary: string
}
