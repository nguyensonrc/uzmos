export interface CreateAppointment {
  customerId?: number
  serviceId?: number
  staffId?: number
  lastDate: Date
  date: Date
  status?: string
  color?: string | "#EEEEEE"
  note?: string
  storeId?: number
  isCheckIn?: boolean | false
  labelId?: number
  remiderSent?: boolean | false
  followUpSent?: boolean | false
  didNotShow: boolean | false
  didNotShowSent: boolean | false
  duration?: number
}
