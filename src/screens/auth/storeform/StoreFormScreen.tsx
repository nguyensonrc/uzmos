import * as React from "react"
import { RouteProp, useRoute } from "@react-navigation/native"
import { Box } from "native-base"
import { TextFieldCustom } from "@components/text-field"
import { color } from "@theme/color"
import { spacing } from "@theme/spacing"
import * as yup from "yup"
import { convertYupErrorInner } from "@utils/yup/yup"
import { ButtonCustom } from "@components/button/buttonCustom"
import { useAuth } from "@hooks/auth"
import { useStores } from "@models/root-store"
import { NavigatorParamList } from "@navigators/auth"
import { navigationRef } from "@navigators/navigation-utilities"
import { isNull } from "lodash"

const formFields = [
  {
    id: "name",
    placeholder: "Bussiness Name",
  },
  {
    id: "categories",
    placeholder: "Categories (Ex: Pizza (Be specific))",
  },
  {
    id: "address",
    placeholder: "Address",
  },
  {
    id: "city",
    placeholder: "City",
  },
  {
    id: "state",
    placeholder: "State",
  },
  {
    id: "zipcode",
    placeholder: "Zipcode",
  },
  {
    id: "phoneNumber",
    placeholder: "Phone Number",
  },
]

const schema = yup.object().shape({
  name: yup.string().required("Name is required"),
  address: yup.string().required("Address is required"),
  city: yup.string().required("City is required"),
  state: yup.string().required("State is required"),
  zipcode: yup.string().required("Zipcode is required"),
  categories: yup.string(),
  phoneNumber: yup.string().required("Phone number is required"),
})

interface StoreFormScreenProps {}

const StoreFormScreen: React.FC<StoreFormScreenProps> = (props) => {
  const { userStore, authStore } = useStores()

  const route = useRoute<RouteProp<NavigatorParamList, "storeForm">>()

  const [storeData, setStoreData] = React.useState({
    name: "",
    address: "",
    city: "",
    state: "",
    zipcode: "",
    categories: "",
    phoneNumber: "",
  })
  const [errors, setErrors] = React.useState({})
  const { register, registerStatus, registerErrorCode, updateCompany, loading } = useAuth()
  React.useEffect(() => {
    if (!isNull(registerErrorCode)) {
      switch (registerErrorCode) {
        default:
          alert("Error, please try again")
      }
    }
  }, [registerErrorCode])

  React.useEffect(() => {
    if (registerStatus) {
      alert("Sign up successful")
    }
  }, [registerStatus])

  const handleFieldChange = (key: string, text: string) => {
    setStoreData((prev) => {
      if (Object.keys(errors).includes(key)) {
        delete errors[key]
      }
      return { ...prev, [key]: text }
    })
  }

  const onSignUp = async () => {
    try {
      const { data: userInfo, providerName } = route.params
      await schema.validate(storeData, { abortEarly: false })
      if (providerName === "email") {
        const signUpData = { ...userInfo, store: storeData, packageId: 2 }
        register(signUpData)
      } else {
        const { company } = userStore.User
        const { token } = authStore.Auth
        updateCompany(storeData, company.id, token)
      }
    } catch (err) {
      setErrors(convertYupErrorInner(err.inner))
    }
  }

  return (
    <Box flex={1} backgroundColor={color.palette.white} paddingX={spacing[2]} alignItems="center">
      {formFields.map(({ id, placeholder }) => (
        <TextFieldCustom
          key={id}
          placeholder={placeholder}
          placeholderTextColor="yellow"
          inputStyle={{ color: color.palette.black }}
          onChangeText={(text) => handleFieldChange(id, text)}
          value={storeData[id]}
          errorMsg={errors[id]}
        />
      ))}
      <ButtonCustom isLoading={loading} onPress={onSignUp}>
        Sign Up
      </ButtonCustom>
    </Box>
  )
}

export default StoreFormScreen
