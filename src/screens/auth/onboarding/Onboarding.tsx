/* eslint-disable react-native/no-color-literals */
/* eslint-disable react-native/no-inline-styles */
import React, { useRef } from "react"
import {
  Dimensions,
  StatusBar,
  View,
  Animated,
  Image,
  StyleSheet,
  TouchableOpacity,
} from "react-native"

import Text from "@components/text"

import styles from "./styles"
import { navigate, navigationRef } from "@navigators/navigation-utilities"
import { useStores } from "@models/root-store/root-store-context"
import { signUpEmailApi } from "@services/api/Auth"
import { SignUpEmail } from "@models/backend/request/Auth"

const { width, height } = Dimensions.get("screen")

const bgs = ["#A5BBFF", "#DDBEFE", "#FF63ED", "#f37952"]
const DATA = [
  {
    key: "3571572",
    title: "Multi-lateral intermediate moratorium",
    description: "I'll back up the multi-byte XSS matrix, that should feed the SCSI application!",
    image: "https://cdn-icons-png.flaticon.com/512/3791/3791216.png",
  },
  {
    key: "3571747",
    title: "Automated radical data-warehouse",
    description: "Use the optical SAS system, then you can navigate the auxiliary alarm!",
    image: "https://cdn-icons-png.flaticon.com/512/7356/7356877.png",
  },
  {
    key: "3571680",
    title: "Inverse attitude-oriented system engine",
    description:
      "The ADP array is down, compress the online sensor so we can input the HTTP panel!",
    image: "https://cdn-icons-png.flaticon.com/512/3789/3789987.png",
  },
  {
    key: "3571603",
    title: "Monitored global data-warehouse",
    description: "We need to program the open-source IB interface!",
    image: "https://cdn-icons-png.flaticon.com/512/2323/2323048.png",
  },
]

const Square = ({ scrollX }: { scrollX: Animated.Value }) => {
  const YOLO = Animated.modulo(
    Animated.divide(Animated.modulo(scrollX, width), new Animated.Value(width)),
    1,
  )

  const rotate = YOLO.interpolate({
    inputRange: [0, 0.5, 1],
    outputRange: ["35deg", "0deg", "35deg"],
  })

  const translateX = YOLO.interpolate({
    inputRange: [0, 0.5, 1],
    outputRange: [0, -height, 0],
  })

  return (
    <Animated.View
      style={[
        styles.square,
        {
          transform: [{ rotate }, { translateX }],
        },
      ]}
    />
  )
}

const Backdrop = ({ scrollX }: { scrollX: Animated.Value }) => {
  const backgroundColor = scrollX.interpolate({
    inputRange: bgs.map((_, i) => i * width),
    outputRange: bgs.map((bg) => bg),
  })

  return <Animated.View style={[StyleSheet.absoluteFillObject, { backgroundColor }]} />
}

const Indicator = ({ scrollX }: { scrollX: Animated.Value }) => {
  return (
    <View style={styles.indicatorView}>
      {DATA.map((_, i) => {
        const inputRange = [(i - 1) * width, i * width, (i + 1) * width]

        const scale = scrollX.interpolate({
          inputRange,
          outputRange: [0.8, 1.4, 0.8],
          extrapolate: "clamp",
        })

        const opacity = scrollX.interpolate({
          inputRange,
          outputRange: [0.6, 0.9, 0.6],
          extrapolate: "clamp",
        })

        return (
          <Animated.View
            key={`indicator-${i}`}
            style={[
              styles.indicator,
              {
                opacity,
                transform: [{ scale }],
              },
            ]}
          ></Animated.View>
        )
      })}
    </View>
  )
}

const handleSkipPress = () => navigationRef.reset({ index: 0, routes: [{ name: "signIn" }] })

const renderItem = ({ item }) => (
  <View style={styles.body}>
    <View style={styles.topView}>
      <Image source={{ uri: item.image }} style={styles.img} />
    </View>
    <View style={{ flex: 0.3 }}>
      <Text style={styles.lblTitle}>{item.title}</Text>
      <Text style={styles.lblDes}>{item.description}</Text>
    </View>
  </View>
)

const SkipButton = () => (
  <View style={styles.btnSkip}>
    <TouchableOpacity hitSlop={styles.btnArea} onPress={handleSkipPress}>
      <Text>{"Skip"}</Text>
    </TouchableOpacity>
  </View>
)

const OnboadingScreen = () => {
  const scrollX = useRef(new Animated.Value(0)).current

  return (
    <View style={styles.container}>
      <StatusBar hidden />
      <Backdrop scrollX={scrollX} />
      <Square scrollX={scrollX} />
      <Animated.FlatList
        data={DATA}
        keyExtractor={(item) => item.key}
        horizontal
        scrollEventThrottle={32}
        onScroll={Animated.event([{ nativeEvent: { contentOffset: { x: scrollX } } }], {
          useNativeDriver: false,
        })}
        contentContainerStyle={styles.flatlistContent}
        showsHorizontalScrollIndicator={false}
        pagingEnabled
        renderItem={renderItem}
      />
      <Indicator scrollX={scrollX} />
      <SkipButton />
    </View>
  )
}

export default OnboadingScreen
