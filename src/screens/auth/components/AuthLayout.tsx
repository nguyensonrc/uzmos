import { AppIcon } from "@assets/"
import React from "react"
import { Keyboard, View } from "react-native"
import FastImage from "react-native-fast-image"
import { styles } from "./styles"

interface AuthLayoutProps {
  children: React.ReactNode
}

const AuthLayout: React.FC<AuthLayoutProps> = ({ children }) => {
  return (
    <View style={styles.container}>
      <View style={styles.appLogoContainer}>
        <FastImage source={AppIcon.imageSource} style={styles.appLogo} />
      </View>
      <View style={styles.mainContainer}>{children}</View>
    </View>
  )
}

export default AuthLayout
