import { TextFieldCustom } from "@components/text-field"
import * as React from "react"
import * as yup from "yup"
import { convertYupErrorInner } from "@utils/yup/yup"
import { ButtonCustom } from "@components/button/buttonCustom"
import { useNavigation } from "@react-navigation/native"
import { authScreenProp } from "@navigators/auth"
import { SignUpEmail } from "@models/backend/request/Auth"
import { AuthLayout } from "../components"

const schema = yup.object().shape({
  email: yup.string().email("Enter a valid email.").required("Email is required."),
  password: yup
    .string()
    .min(5, "Password must be at least 5 characters long.")
    .max(12, "Password maximum length is 12 characters.")
    .matches(/^(?=.[0-9])[a-zA-Z0-9!@#$%^&]{6,12}$/)
    .required("Password is required."),
  confirmPassword: yup
    .string()
    .test({
      message: "The confirmation password is not matched",
      test: (value, context) => {
        const { password, confirmPassword } = context.parent
        if (password === confirmPassword) {
          return true
        }
        return false
      },
    })
    .required("Confirmation password is required."),
})

const SignUpScreen = () => {
  const navigation = useNavigation<authScreenProp>()
  const [formData, setFormData] = React.useState({
    email: "",
    password: "",
    confirmPassword: "",
    fullName: "",
  })
  const [errors, setErrors] = React.useState<{
    fullName?: string
    email?: string
    password?: string
    confirmPassword?: string
  }>({})

  const handleFormDataChange = (key: string, value: string) => {
    setFormData((prev) => {
      if (Object.keys(errors).includes(key)) {
        delete errors[key]
      }
      return { ...prev, [key]: value }
    })
  }

  const onNext = async () => {
    try {
      await schema.validate(formData, { abortEarly: false })
      const data = {
        fullName: formData.fullName,
        email: formData.email,
        password: formData.password,
      } as SignUpEmail
      navigation.navigate("storeForm", { data, providerName: "email" })
    } catch (err) {
      setErrors(convertYupErrorInner(err.inner))
    }
  }

  return (
    <AuthLayout>
      <>
        <TextFieldCustom
          errorMsg={errors?.fullName}
          onChangeText={(value) => handleFormDataChange("fullName", value)}
          placeholder="Name"
        />
        <TextFieldCustom
          errorMsg={errors?.email}
          onChangeText={(value) => handleFormDataChange("email", value)}
          placeholder="Email"
        />
        <TextFieldCustom
          errorMsg={errors?.password}
          onChangeText={(value) => handleFormDataChange("password", value)}
          isPassword
          placeholder="Password"
        />
        <TextFieldCustom
          errorMsg={errors?.confirmPassword}
          onChangeText={(value) => handleFormDataChange("confirmPassword", value)}
          isPassword
          placeholder="Confirm Password"
        />
        <ButtonCustom onPress={onNext}>Next</ButtonCustom>
      </>
    </AuthLayout>
  )
}

export default SignUpScreen
