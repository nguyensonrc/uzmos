/* eslint-disable react/jsx-no-duplicate-props */
import { mainScreenProp } from "@navigators/main"
import { useFocusEffect, useNavigation } from "@react-navigation/native"
import moment from "moment"
import { Fab } from "native-base"
import * as React from "react"
import { useCallback, useRef, useState } from "react"
import { Dimensions, Text, TouchableOpacity, View } from "react-native"
import { DateData } from "react-native-calendars"

import { CustomCalendarList } from "@components/calendar/CustomCalendarList"
import EventCalendar from "@components/event-calendar/EventCalendar.js"
import CustomModal, { IRefCustomModal } from "@components/modal/CustomModal"
import { Screen } from "@components/screen/screen"
import VectorIcon from "@components/vectorIcon/vectorIcon"
import { CALENDAR_FORMAT, DATE_FORMAT } from "@config/constants"
import { useAppointment } from "@hooks/appointment/useAppointment"

import { color } from "@theme/color"
import styles from "./styles"

const { width } = Dimensions.get("window")

const eventss = [
  {
    start: "2022-09-07 00:30:00",
    end: "2022-09-07 01:30:00",
    title: "Dr. Mariana Joseph",
    summary: "3412 Piedmont Rd NE, GA 3032",
  },
  {
    start: "2022-09-07 00:30:00",
    end: "2022-09-07 01:30:00",
    title: "22Dr. Mariana Joseph",
    summary: "223412 Piedmont Rd NE, GA 3032",
  },
  {
    start: "2022-09-07 01:30:00",
    end: "2022-09-07 02:20:00",
    title: "Dr. Mariana Joseph",
    summary: "3412 Piedmont Rd NE, GA 3032",
  },
  {
    start: "2022-09-07 04:10:00",
    end: "2022-09-07 04:40:00",
    title: "Dr. Mariana Joseph",
    summary: "3412 Piedmont Rd NE, GA 3032",
  },
  {
    start: "2022-09-07 01:05:00",
    end: "2022-09-07 01:45:00",
    title: "Dr. Mariana Joseph",
    summary: "3412 Piedmont Rd NE, GA 3032",
  },
  {
    start: "2022-09-07 14:30:00",
    end: "2022-09-07 16:30:00",
    title: "Dr. Mariana Joseph",
    summary: "3412 Piedmont Rd NE, GA 3032",
  },
  {
    start: "2022-09-08 01:20:00",
    end: "2022-09-08 02:20:00",
    title: "Dr. Mariana Joseph",
    summary: "3412 Piedmont Rd NE, GA 3032",
  },
  {
    start: "2022-09-08 04:10:00",
    end: "2022-09-08 04:40:00",
    title: "Dr. Mariana Joseph",
    summary: "3412 Piedmont Rd NE, GA 3032",
  },
  {
    start: "2022-09-08 00:45:00",
    end: "2022-09-08 01:45:00",
    title: "Dr. Mariana Joseph",
    summary: "3412 Piedmont Rd NE, GA 3032",
  },
  {
    start: "2022-09-08 11:30:00",
    end: "2022-09-08 12:30:00",
    title: "Dr. Mariana Joseph",
    summary: "3412 Piedmont Rd NE, GA 3032",
  },
  {
    start: "2022-09-09 01:30:00",
    end: "2022-09-09 02:00:00",
    title: "Dr. Mariana Joseph",
    summary: "3412 Piedmont Rd NE, GA 3032",
  },
  {
    start: "2022-09-09 03:10:00",
    end: "2022-09-09 03:40:00",
    title: "Dr. Mariana Joseph",
    summary: "3412 Piedmont Rd NE, GA 3032",
  },
  {
    start: "2022-09-09 00:10:00",
    end: "2022-09-09 01:45:00",
    title: "Dr. Mariana Joseph",
    summary: "3412 Piedmont Rd NE, GA 3032",
  },
]

const HomeScreen = () => {
  const navigation = useNavigation<mainScreenProp>()
  const modalRef = useRef<IRefCustomModal>(null)
  const [currentDate, setCurrentDate] = useState(moment())
  const contentCalendarRef = useRef(null)

  const { getListAppointment, listAppointmnetTemp } = useAppointment()

  useFocusEffect(
    useCallback(() => {
      getListAppointment()
    }, []),
  )

  const _eventTapped = (event: any) => {
    console.log("dasd", event)
  }

  const _dateChanged = (date: string) => {
    setCurrentDate(moment(date))
  }

  const leftAction = () => {
    const prev = moment(currentDate.format(DATE_FORMAT).toString()).add(-1, "days")
    setCurrentDate(prev)
  }
  const rightAction = () => {
    const next = moment(currentDate.format(DATE_FORMAT).toString()).add(1, "days")
    setCurrentDate(next)
  }

  const openCalendar = () => {
    if (modalRef && modalRef.current) {
      modalRef.current.openModal()
    }
  }

  const selectedDate = (date: DateData) => {
    try {
      if (modalRef && modalRef.current) {
        modalRef.current.closeModal()
      }
      if (date && date.timestamp) {
        setCurrentDate(moment(date.timestamp))
      }
    } catch (error) {
      __DEV__ && console.log({ error })
    }
  }

  const RenderHeader = () => (
    <>
      <View style={styles.container}>
        <View style={styles.left}>
          <VectorIcon
            style={styles.icon}
            size={24}
            iconSet="ion"
            name="chatbox-ellipses-outline"
            onPress={leftAction}
          />
        </View>
        <View style={styles.body}>
          <TouchableOpacity onPress={leftAction}>
            <VectorIcon style={styles.icon} size={24} iconSet="ion" name="chevron-back-outline" />
          </TouchableOpacity>
          <TouchableOpacity onPress={openCalendar}>
            <Text style={styles.txtDate}>{currentDate.format(CALENDAR_FORMAT)}</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={rightAction}>
            <VectorIcon
              id={"1"}
              style={styles.icon}
              size={24}
              iconSet="ion"
              name="chevron-forward-outline"
            />
          </TouchableOpacity>
        </View>
        <View style={styles.right}>
          <VectorIcon
            id={"2"}
            style={styles.icon}
            size={24}
            iconSet="ion"
            name="filter"
            onPress={rightAction}
          />
        </View>
      </View>
      <CustomModal
        ref={modalRef}
        childView={<CustomCalendarList currentDate={currentDate} selectedDate={selectedDate} />}
      />
    </>
  )

  const RenderBody = () => (
    <EventCalendar
      ref={contentCalendarRef}
      eventTapped={_eventTapped}
      dateChanged={_dateChanged}
      events={listAppointmnetTemp}
      width={width}
      initDate={currentDate.format(DATE_FORMAT).toString()}
      scrollToFirst
      upperCaseHeader
      uppercase
      format24h={true}
    />
  )

  const RenderNewApppointment = useCallback(() => {
    return (
      <Fab
        position={"absolute"}
        bottom={50}
        onPress={() => navigation.navigate("newAppointment")}
        _pressed={{ opacity: 0.2 }}
        h={16}
        w={16}
        label={"+"}
        size="lg"
        rounded={"full"}
        backgroundColor={color.palette.orange}
        renderInPortal={false}
      />
    )
  }, [])

  return (
    <Screen style={styles.content}>
      <RenderHeader />
      <RenderBody />
      <RenderNewApppointment />
    </Screen>
  )
}

export default HomeScreen
