import { color } from "@theme/color"
import { spacing } from "@theme/spacing"
import { Dimensions, StyleSheet } from "react-native"

const { width } = Dimensions.get("window")

const styles = StyleSheet.create({
  body: {
    alignItems: "center",
    flex: 2,
    flexDirection: "row",
    justifyContent: "space-evenly",
  },
  calendarList: { overflow: "hidden", width },
  container: {
    flex: 0,
    flexDirection: "row",
    justifyContent: "center",
    paddingHorizontal: spacing[4],
    paddingTop: spacing[2],
  },
  content: { flex: 1 },
  icon: { backgroundColor: color.palette.white },
  left: { alignItems: "flex-start", flex: 0.7 },
  right: { alignItems: "flex-end", flex: 0.7 },
  txtDate: { color: color.palette.black, fontSize: 15, fontWeight: "800" },
})

export default styles
