import { Fab, FlatList } from "native-base"
import React, { useCallback } from "react"
import { ListRenderItemInfo } from "react-native"

import { ButtonCustom, Header, Screen } from "@components/index"
import Text from "@components/text/text"
import { useStaff } from "@hooks/staff"
import { StaffDTO } from "@models/backend/response/Staff"
import { goBack, navigate } from "@navigators/navigation-utilities"
import { useFocusEffect } from "@react-navigation/native"
import { color } from "@theme/color"

interface StaffListScreenProps {}

const StaffListScreen = (props: StaffListScreenProps) => {
  const { getStaff, staffs } = useStaff()

  useFocusEffect(
    useCallback(() => {
      getStaff()
    }, []),
  )

  const _renderItem = ({ item }: ListRenderItemInfo<StaffDTO>) => {
    const { name } = item

    const onStaffPress = () => {
      // handle staff press event
      navigate("staffProfile", { detail: item })
    }

    return (
      <ButtonCustom
        onPress={onStaffPress}
        rounded={"none"}
        w="full"
        backgroundColor={color.palette.white}
        borderBottomColor={color.palette.lighterGrey}
        borderBottomWidth={"1"}
        justifyContent="flex-start"
      >
        <Text text={name} marginY="1" />
      </ButtonCustom>
    )
  }

  return (
    <Screen>
      <Header headerTx={"screens.headerTitle.staffList"} onLeftPress={goBack} />
      <FlatList
        data={staffs}
        keyExtractor={(item) => item.id.toString()}
        renderItem={_renderItem}
      />
      <Fab
        bottom={50}
        onPress={() => navigate("staffProfile", { editable: true })}
        _pressed={{ opacity: 0.2 }}
        h={16}
        w={16}
        label={"+"}
        size="lg"
        rounded={"full"}
        backgroundColor={color.palette.black}
        renderInPortal={false}
      />
    </Screen>
  )
}

export default StaffListScreen
