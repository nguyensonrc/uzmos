import { RouteProp, useNavigation, useRoute } from "@react-navigation/native"
import { get, isEmpty, isMatch } from "lodash"
import { FlatList } from "native-base"
import React, { useRef, useState } from "react"
import { Image } from "react-native-image-crop-picker"
import * as yup from "yup"

import { Avatar, ButtonCustom, Header, Screen } from "@components/index"
import CustomModal, { IRefCustomModal } from "@components/modal/CustomModal"
import ImagePicker from "@components/modal/ImagePicker"
import { TextFieldCustom } from "@components/text-field"
import Text from "@components/text/text"
import { useStaff } from "@hooks/staff"
import { useUtility } from "@hooks/utility"
import { TxKeyPath } from "@i18n/i18n"
import { StaffDTO } from "@models/backend/response/Staff"
import { mainScreenProp, NavigatorParamList } from "@navigators/main"
import { goBack } from "@navigators/navigation-utilities"
import { color } from "@theme/color"
import { spacing } from "@theme/spacing"
import { convertYupErrorInner } from "@utils/yup/yup"
import { Alert } from "react-native"

const schema = yup.object().shape({
  name: yup.string().trim().required("Name is required"),
  avatar: yup.string().required("Avatar is required"),
  phoneNumber: yup
    .string()
    .matches(/\d+/g, "Phone number format is not correct")
    .required("Phone number is required"),
  email: yup.string().email("Email format is not correct").nullable(),
})

const staffFields = [
  { id: "name", label: "name" },
  { id: "phoneNumber", label: "phoneNumber" },
  { id: "email", label: "email", isOptional: true },
  { id: "workingDays", label: "Working Days", isOptional: true },
  { id: "breaks", label: "Breaks", isOptional: true },
  { id: "timeOff", label: "Time Off", isOptional: true },
]

interface StaffProfileScreenProps {}

const StaffProfileScreen = (props: StaffProfileScreenProps) => {
  const route = useRoute<RouteProp<NavigatorParamList, "staffProfile">>()
  const { detail, editable } = route.params
  const [isEditable, setEditable] = useState<boolean>(editable || false)
  const [isDiff, setIsDiff] = useState<boolean>(false)
  const [errors, setErrors] = useState({})
  const {
    loading,
    loadingDelete,
    createStaffProfile,
    editStaffProfile,
    editStaffStatus,
    createStaffStatus,
    deleteStaffStatus,
    deleteStaffProfile,
  } = useStaff()
  const { uploadingImage, loading: uploading, imageData } = useUtility()
  const staffRef = useRef<Partial<StaffDTO>>({})
  const imagePickerRef = useRef<IRefCustomModal>(null)
  const navigation = useNavigation<mainScreenProp>()
  React.useEffect(() => {
    if (createStaffStatus) {
      alert("Create staff successful !")
      goBack()
    }
  }, [createStaffStatus])

  React.useEffect(() => {
    if (editStaffStatus) {
      alert("Update staff successful !")
      goBack()
    }
  }, [editStaffStatus])

  React.useEffect(() => {
    if (deleteStaffStatus) {
      alert("Delete staff successful !")
      goBack()
    }
  }, [deleteStaffStatus])

  const onAvatarPress = () => {
    if (imagePickerRef?.current) {
      imagePickerRef.current.openModal()
    }
  }

  const onImageSelect = (data: Image) => {
    if (imagePickerRef.current) {
      handleProfileChange("avatar", data.path)
      uploadingImage(data)
      imagePickerRef.current.closeModal()
    }
  }

  const onFocusAction = (id: string) => {
    try {
      if (id && id === "workingDays") {
        navigation.navigate("workingDays", { staffDetail: detail })
      } else if (id && id === "breaks") {
        navigation.navigate("breaks", { staffDetail: detail })
      } else if (id && id === "timeOff") {
        navigation.navigate("timeOff", { staffDetail: detail })
      }
    } catch (error) {
      __DEV__ && console.log(error)
    }
  }

  const handleProfileChange = (id: string, value: string) => {
    switch (id) {
      default:
        staffRef.current[id] = value
    }
    setIsDiff(!isMatch(detail, staffRef.current))
  }

  const handleProfileButtonPress = async () => {
    try {
      const invokingData = {
        ...detail,
        ...staffRef.current,
        avatar: imageData?.url || detail?.avatar,
      }

      switch (screenStatus) {
        case "cancel":
          return setEditable(false)
        case "edit":
          return setEditable(true)
        case "save":
          await schema.validate(invokingData, { abortEarly: false })
          editStaffProfile(invokingData)
          break
        case "create":
          await schema.validate(invokingData, { abortEarly: false })
          createStaffProfile(invokingData)
          break
      }
    } catch (err) {
      if (err?.inner && !isEmpty(err.inner)) {
        setErrors(convertYupErrorInner(err.inner))
      }
    }
  }

  const setData = (detail: StaffDTO, item) => {
    if (item === "workingDays") {
      let days = ""
      if (detail.workingHours && detail.workingHours.length > 0) {
        detail.workingHours.forEach((ele) => {
          if (ele.day === 0 && ele.open) {
            days += "Su "
          }
          if (ele.day === 1 && ele.open) {
            days += "M "
          }
          if (ele.day === 2 && ele.open) {
            days += "T "
          }
          if (ele.day === 3 && ele.open) {
            days += "W "
          }
          if (ele.day === 4 && ele.open) {
            days += "Th "
          }
          if (ele.day === 5 && ele.open) {
            days += "F "
          }
          if (ele.day === 6 && ele.open) {
            days += "S"
          }
        })
        return days
      } else {
        return ""
      }
    }

    return get(detail, item)
  }

  const handleDeleteButtonPress = async () => {
    Alert.alert("Warring", "Do you want to delete " + detail.name + " ?", [
      {
        text: "Cancel",
        onPress: () => console.log("Cancel Pressed"),
        style: "cancel",
      },
      { text: "OK", onPress: () => deleteStaffProfile(detail.id) },
    ])
  }

  const screenStatus = React.useMemo<"save" | "cancel" | "edit" | "create">(() => {
    return isEditable ? (isEmpty(detail) ? "create" : isDiff ? "save" : "cancel") : "edit"
  }, [staffRef, isDiff, isEditable])

  const renderHeaderComponent = () => (
    <Avatar
      source={{
        uri: staffRef.current?.avatar ? staffRef.current.avatar : detail?.avatar,
        cache: "force-cache",
      }}
      errorMsg={errors.avatar}
      disabled={!isEditable}
      onPress={onAvatarPress}
      isLoading={uploading}
    />
  )

  const renderItem = ({ item, index }) => (
    <TextFieldCustom
      {...item}
      // keyboardType={item.keyboardType as KeyboardTypeOptions}
      style={{ marginHorizontal: spacing[4] }}
      opacity={isEditable ? 1 : 0.6}
      paddingLeft={"1.5"}
      alignSelf="center"
      rounded={"md"}
      key={item.id}
      labelTx={`textInput.label.${item.id}` as TxKeyPath}
      defaultValue={get(detail, item.id)}
      editable={isEditable}
      onChangeText={(value) => handleProfileChange(item.id, value)}
      errorMsg={errors[item.id]}
    />
  )

  const renderFooterComponent = React.useCallback(() => {
    return (
      <>
        <ButtonCustom
          isLoading={loading}
          disabled={uploading}
          w="90%"
          marginBottom={spacing[2]}
          onPress={handleProfileButtonPress}
        >
          <Text tx={`button.${screenStatus}`} style={{ color: color.palette.white }} />
        </ButtonCustom>
        {detail?.id && (
          <ButtonCustom
            isLoading={loadingDelete}
            disabled={loadingDelete}
            w="90%"
            marginBottom={spacing[2]}
            onPress={handleDeleteButtonPress}
          >
            <Text text={"Delete"} style={{ color: color.palette.white }} />
          </ButtonCustom>
        )}
      </>
    )
  }, [screenStatus, loading, uploading, loadingDelete])

  return (
    <Screen>
      <Header headerText={detail?.name} leftIcon="back" onLeftPress={goBack} />
      <FlatList
        data={staffFields}
        ListHeaderComponent={renderHeaderComponent}
        keyExtractor={(item) => item.id}
        renderItem={({ item }) => (
          <TextFieldCustom
            {...item}
            isFocused={item.id === "workingDays"}
            buttonClick={() => onFocusAction(item.id)}
            isHasButton={item.id === "workingDays" || item.id === "breaks" || item.id === "timeOff"}
            style={{ marginHorizontal: spacing[4] }}
            opacity={isEditable ? 1 : 0.6}
            paddingLeft={"1.5"}
            alignSelf="center"
            rounded={"md"}
            key={item.id}
            labelTx={`textInput.label.${item.id}` as TxKeyPath}
            defaultValue={setData(detail, item.id)}
            editable={isEditable}
            onChangeText={(value) => handleProfileChange(item.id, value)}
            errorMsg={errors[item.id]}
          />
        )}
      />
      {renderFooterComponent()}

      <CustomModal ref={imagePickerRef} childView={<ImagePicker onImageSelect={onImageSelect} />} />
    </Screen>
  )
}

export default StaffProfileScreen
