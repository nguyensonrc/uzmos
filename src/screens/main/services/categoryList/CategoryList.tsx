import { mainScreenProp } from "@navigators/main"
import { useFocusEffect, useNavigation } from "@react-navigation/native"
import { Fab } from "native-base"
import * as React from "react"
import { useCallback } from "react"
import { Alert, FlatList, TouchableOpacity, View } from "react-native"
import { Header } from "@components/header/header"
import Loading from "@components/loading/Loading"
import { Screen } from "@components/screen/screen"
import SearchBar from "@components/search-bar"
import { RefSearch } from "@components/search-bar/SearchBar"
import Text from "@components/text"
import { useService } from "@hooks/service/useService"
import { CategoryDTO } from "@models/backend/response/Service"
import { color } from "@theme/color"
import styles from "./styles"

const Item = ({ item }: { item: CategoryDTO }) => (
  <View style={styles.item}>
    <View style={styles.subContainerItem}>
      <View style={styles.viewNamePrice}>
        <Text ellipsizeMode="tail" numberOfLines={1} style={styles.serviceName}>
          {item.name}
        </Text>
      </View>
    </View>
  </View>
)

const CategoryListScreen = () => {
  const ref = React.useRef<RefSearch>(null)
  const navigation = useNavigation<mainScreenProp>()

  const { loadingCatList, getCatList, catList } = useService()

  useFocusEffect(
    useCallback(() => {
      getCatList("")
    }, []),
  )

  const cancelAction = () => {
    getCatList("")
  }

  const searchAction = () => {
    if (ref && ref.current) {
      getCatList(ref.current.searchPhrase.trim())
    }
  }

  const goDetail = (detail: CategoryDTO) => {
    navigation.navigate("categoryDetail", { detail, editable: false })
    console.log("Category Detail name " + detail.name)
  }

  const renderItem = ({ item }: { item: CategoryDTO }) => {
    return (
      <TouchableOpacity onPress={() => goDetail(item)}>
        <Item item={item} />
      </TouchableOpacity>
    )
  }

  const renderEmpty = useCallback(() => {
    return (
      <View style={styles.empty}>
        <Text tx="common.empty" />
      </View>
    )
  }, [])

  const renderCatList = useCallback(() => {
    return (
      <View style={styles.viewServiceList}>
        {loadingCatList ? (
          <Loading color={"black"} />
        ) : catList.length === 0 ? (
          renderEmpty()
        ) : (
          <FlatList
            data={catList}
            renderItem={renderItem}
            keyExtractor={(item) => item.id.toString()}
            showsVerticalScrollIndicator={false}
          />
        )}
      </View>
    )
  }, [catList, loadingCatList])

  const renderNewCat = useCallback(() => {
    return (
      <Fab
        position={"absolute"}
        bottom={50}
        onPress={() => navigation.navigate("newCategory")}
        _pressed={{ opacity: 0.2 }}
        h={16}
        w={16}
        label={"+"}
        size="lg"
        rounded={"full"}
        backgroundColor={color.palette.black}
        renderInPortal={false}
      />
    )
  }, [])
  return (
    <Screen style={styles.body}>
      <Header headerText={"Category"} leftIcon="back" onLeftPress={() => Alert.alert("left nav")} />
      <SearchBar searchAction={searchAction} cancelAction={cancelAction} ref={ref} />
      {renderCatList()}
      {renderNewCat()}
    </Screen>
  )
}

export default CategoryListScreen
