import { ButtonCustom, Header, Screen } from "@components/index"
import { TextFieldCustom } from "@components/text-field"
import Text from "@components/text/text"
import VectorIcon from "@components/vectorIcon/vectorIcon"
import { useService } from "@hooks/service/useService"
import { TxKeyPath } from "@i18n/i18n"
import { EditCategory } from "@models/backend/request/Service"
import { ServiceInCategoryDTO } from "@models/backend/response/Service"
import { NavigatorParamList } from "@navigators/main"
import { goBack } from "@navigators/navigation-utilities"
import { RouteProp, useFocusEffect, useRoute } from "@react-navigation/native"
import { color } from "@theme/color"
import { spacing } from "@theme/spacing"
import { convertYupErrorInner } from "@utils/yup/yup"
import { get, isEmpty, isMatch } from "lodash"
import { FlatList, View } from "native-base"
import React, { useCallback, useRef, useState } from "react"
import { TouchableOpacity } from "react-native"
import * as yup from "yup"
import styles from "./styles"

const schema = yup.object().shape({
  name: yup.string().trim().required("Name is required"),
})

const categoryFields = [{ id: "name", label: "Category name" }]

interface CategoryDetailScreenProps {}

const CategoryDetail = (props: CategoryDetailScreenProps) => {
  const route = useRoute<RouteProp<NavigatorParamList, "categoryDetail">>()
  const { detail, editable } = route.params
  const [isEditable, setEditable] = useState<boolean>(editable)
  const [textAll, setTextAll] = useState("Sellect All")
  const [Count, setCount] = useState(0)
  const [isDiff, setIsDiff] = useState<boolean>(false)
  const [errors, setErrors] = useState({})

  const {
    loadingNewCategory,
    editCategory,
    editCategoryStatus,
    getListServiceInCategory,
    serviceInCategory,
    setNewListServerInCategory,
    updateListServerInCategory,
  } = useService()
  const categoryRef = useRef<Partial<EditCategory>>({})
  useFocusEffect(
    useCallback(() => {
      getListServiceInCategory(detail.id)
    }, []),
  )
  React.useEffect(() => {
    if (editCategoryStatus) {
      alert("Update Category successful !")
      goBack()
    }
  }, [editCategoryStatus])
  React.useEffect(() => {
    if (serviceInCategory) {
      console.log("serviceInCategory successful ! " + serviceInCategory.length)
      for (let index = 0; index < serviceInCategory.length; index++) {
        if (serviceInCategory[index].selected === 1) {
          setCount(Count + 1)
        }
        
      }
      // goBack()
    }
  }, [])
  const handleProfileChange = (id: string, value: string) => {
    switch (id) {
      default:
        categoryRef.current[id] = value
    }
    setIsDiff(!isMatch(detail, categoryRef.current))
  }
  const handleProfileButtonPress = async () => {
    try {
      const invokingData = {
        ...detail,
        ...categoryRef.current,
      }
      switch (screenStatus) {
        case "cancel":
          return setEditable(false)
        case "edit":
          return setEditable(true)
        case "save":
          await schema.validate(invokingData, { abortEarly: false })
          editCategory(invokingData)
          updateListServerInCategory(serviceInCategory, detail.id)
          break
      }
    } catch (err) {
      if (err?.inner && !isEmpty(err.inner)) {
        setErrors(convertYupErrorInner(err.inner))
      }
    }
  }

  const screenStatus = React.useMemo<"save" | "cancel" | "edit" | "create">(() => {
    return isEditable ? (isEmpty(detail) ? "create" : isDiff ? "save" : "cancel") : "edit"
  }, [categoryRef, isDiff, isEditable])

  const renderFooterComponent = React.useCallback(() => {
    return (
      <ButtonCustom
        isLoading={loadingNewCategory}
        disabled={loadingNewCategory}
        w="90%"
        marginBottom={spacing[2]}
        marginTop={spacing[1]}
        onPress={handleProfileButtonPress}
      >
        <Text tx={`button.${screenStatus}`} style={{ color: color.palette.white }} />
      </ButtonCustom>
    )
  }, [screenStatus, loadingNewCategory])

  const renderItem = ({ item, index }: { item: ServiceInCategoryDTO; index: number }) => {
    return (
      <TouchableOpacity
        disabled={!isEditable}
        activeOpacity={0.7}
        onPress={() => handleSecletCheck(item, index)}
      >
        <Item item={item} />
      </TouchableOpacity>
    )
  }
  const Item = ({ item }: { item: ServiceInCategoryDTO }) => (
    <View style={styles.item}>
      <View style={styles.subContainerItem}>
        <View style={styles.viewNamePrice}>
          <Text ellipsizeMode="tail" numberOfLines={1} style={styles.serviceName}>
            {item.name}
          </Text>
          {item?.selected === 1 && (
            <VectorIcon
              color={color.palette.black}
              size={20}
              iconSet="ion"
              name="checkmark-outline"
            />
          )}
        </View>
      </View>
    </View>
  )
  const handleSecletCheck = (item: ServiceInCategoryDTO, index: number) => {
    const tempList = serviceInCategory
    if (tempList[index].selected === 0) {
      tempList[index].selected = 1
      tempList[index].addNew = 1
    } else {
      tempList[index].selected = 0
      tempList[index].addNew = 1
    }
    setNewListServerInCategory(tempList)
    if (tempList[index].selected === 0) {
      setCount(Count - 1)
    } else {
      setCount(Count + 1)
    }
    setIsDiff(true)
  }
  const renderSelectAll = () => {
    return (
      <TouchableOpacity disabled={!isEditable} onPress={handleSecletAll}>
        <View style={styles.itemSelectAll}>
          <View style={styles.subContainerItem}>
            <View style={styles.viewNamePrice}>
              <Text ellipsizeMode="tail" numberOfLines={1} style={styles.serviceName}>
                {textAll}
              </Text>
              <Text style={styles.servicePrice}>{"Counts " + Count}</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    )
  }
  const handleSecletAll = () => {
    const tempList = serviceInCategory
    for (let index = 0; index < serviceInCategory.length; index++) {
      if (textAll === "Sellect All") {
        tempList[index].selected = 1
        tempList[index].addNew = 1
      } else {
        tempList[index].selected = 0
        tempList[index].addNew = 1
      }
    }
    setNewListServerInCategory(tempList)
    if (textAll === "Sellect All") {
      setTextAll("Un Sellect All")
      setCount(serviceInCategory.length)
    } else {
      setTextAll("Sellect All")
      setCount(0)
    }
    setIsDiff(true)
  }
  return (
    <Screen>
      <Header headerText="Category" leftIcon="back" onLeftPress={goBack} />
      <FlatList
        data={categoryFields}
        keyExtractor={(item) => item.id}
        renderItem={({ item, index }) => (
          <TextFieldCustom
            {...item}
            style={{ marginHorizontal: spacing[4] }}
            opacity={isEditable ? 1 : 0.6}
            paddingLeft={"1.5"}
            alignSelf="center"
            rounded={"md"}
            key={item.id}
            labelTx={`textInput.label.${item.id}` as TxKeyPath}
            defaultValue={get(detail, item.id)}
            label={item.label}
            editable={isEditable}
            onChangeText={(value) => handleProfileChange(item.id, value)}
            errorMsg={errors[item.id]}
          />
        )}
      />
      <Text
        style={styles.serviceName}
        ellipsizeMode="tail"
        numberOfLines={1}
        text={"List Service"}
        marginY="3"
      />
      <FlatList
        data={serviceInCategory}
        ListHeaderComponent={renderSelectAll}
        keyExtractor={(item) => item.service_id.toString()}
        renderItem={renderItem}
      />
      {renderFooterComponent()}
    </Screen>
  )
}

export default CategoryDetail
