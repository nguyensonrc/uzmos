import { ButtonCustom } from "@components/button/buttonCustom"
import { Header } from "@components/header/header"
import { Avatar } from "@components/index"
import CustomModal, { IRefCustomModal } from "@components/modal/CustomModal"
import ImagePicker from "@components/modal/ImagePicker"
import { Screen } from "@components/screen/screen"
import Text from "@components/text"
import { TextFieldCustom } from "@components/text-field"
import { useService } from "@hooks/service/useService"
import { useUtility } from "@hooks/utility"
import { NewService } from "@models/backend/request/Service"
import { mainScreenProp } from "@navigators/main"
import { useNavigation } from "@react-navigation/native"
import { color } from "@theme/color"
import { spacing } from "@theme/spacing"
import { convertYupErrorInner } from "@utils/yup/yup"
import { get } from "lodash"
import * as React from "react"
import { useCallback, useEffect, useRef, useState } from "react"
import { Alert, FlatList, KeyboardTypeOptions } from "react-native"
import { Image } from "react-native-image-crop-picker"
import * as yup from "yup"

const profileFields = [
  { id: "name", key: "name", label: "Service name", keyboardType: "default" },
  { id: "serviceDuration", key: "serviceDuration", label: "Duration", keyboardType: "numeric" },
  { id: "cost", key: "cost", label: "Cost", keyboardType: "numeric" },
  { id: "color", key: "color", label: "Color", isOptional: true, keyboardType: "default" },
]

const schema = yup.object().shape({
  cost: yup.number().required("Cost is required"),
  serviceDuration: yup.number().required("Duration is required"),
  name: yup.string().required("Service name is required"),
})

const NewServiceScreen = () => {
  const [errors, setErrors] = useState({})
  const serviceRef = useRef<Partial<NewService>>({})
  const navigation = useNavigation<mainScreenProp>()

  const imagePickModalRef = useRef<IRefCustomModal>()

  const { addNewService, loadingNewService, errNewService, newService } = useService()
  const { loading: uploading, imageData, uploadingImage } = useUtility()

  useEffect(() => {
    if (newService) {
      navigation.goBack()
    }
  }, [newService])

  useEffect(() => {
    return () => {
      if (serviceRef && serviceRef.current) {
        serviceRef.current = null
      }
    }
  }, [])

  useEffect(() => {
    if (errNewService) {
      alert(errNewService.message)
    }
  }, [errNewService])

  const handleImageSelect = (image: Image) => {
    if (serviceRef?.current && image?.path) {
      serviceRef.current.photo = image.path
      uploadingImage(image)
      imagePickModalRef.current.closeModal()
    }
  }

  const handleProfileChange = (id: string, value: string) => {
    switch (id) {
      case "name":
        serviceRef.current.name = value
        break
      case "serviceDuration":
        serviceRef.current.serviceDuration = parseInt(value)
        break
      case "cost":
        serviceRef.current.cost = parseInt(value)
        serviceRef.current.price = parseInt(value)
        break
      default:
        break
    }
  }

  const handleButtonPress = async () => {
    if (serviceRef.current) {
      try {
        const result = await schema.validate(serviceRef.current, { abortEarly: false })
        if (result) {
          setErrors({})
          if (imageData?.url) {
            serviceRef.current.photo = imageData.url
          }
          addNewService(serviceRef.current)
        }
      } catch (error) {
        setErrors(convertYupErrorInner(error.inner))
      }
    }
  }

  const renderFooterComponent = useCallback(() => {
    return (
      <ButtonCustom
        disabled={loadingNewService || uploading}
        isLoading={loadingNewService}
        w="90%"
        marginBottom={spacing[2]}
        onPress={handleButtonPress}
      >
        <Text tx="button.save" style={{ color: color.palette.white }} />
      </ButtonCustom>
    )
  }, [loadingNewService, uploading])

  const onAvatarPress = () => {
    if (imagePickModalRef?.current) {
      imagePickModalRef.current.openModal()
    }
  }

  const renderHeaderComponent = () => {
    return (
      <Avatar
        source={{
          uri: serviceRef.current?.photo ? serviceRef.current.photo : null,
          cache: "force-cache",
        }}
        onPress={onAvatarPress}
        isLoading={uploading}
      />
    )
  }

  return (
    <Screen>
      <Header
        headerTx="service.services"
        leftIcon="back"
        onLeftPress={() => Alert.alert("left nav")}
      />
      <FlatList
        data={profileFields}
        showsVerticalScrollIndicator={false}
        ListHeaderComponent={renderHeaderComponent}
        keyExtractor={(item) => item.id}
        renderItem={({ item }) => {
          return (
            <TextFieldCustom
              {...item}
              keyboardType={item.keyboardType as KeyboardTypeOptions}
              style={{ marginHorizontal: spacing[4] }}
              opacity={1}
              paddingLeft={"1.5"}
              alignSelf="center"
              rounded={"md"}
              key={item.id}
              label={item.label}
              value={get(serviceRef.current, item.key)}
              editable={true}
              onChangeText={(value) => handleProfileChange(item.id, value)}
              errorMsg={errors[item.id]}
            />
          )
        }}
      />
      {renderFooterComponent()}
      <CustomModal
        ref={imagePickModalRef}
        childView={<ImagePicker onImageSelect={handleImageSelect} />}
      />
    </Screen>
  )
}

export default NewServiceScreen
