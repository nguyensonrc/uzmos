import { ButtonCustom } from "@components/button/buttonCustom"
import { Header } from "@components/header/header"
import { Screen } from "@components/screen/screen"
import Text from "@components/text"
import { TextFieldCustom } from "@components/text-field"
import { useService } from "@hooks/service/useService"
import { NewService } from "@models/backend/request/Service"
import { ServiceDTO } from "@models/backend/response/Service"
import { mainScreenProp } from "@navigators/main"
import { useNavigation } from "@react-navigation/native"
import { color } from "@theme/color"
import { spacing } from "@theme/spacing"
import { convertYupErrorInner } from "@utils/yup/yup"
import { Avatar } from "native-base"
import * as React from "react"
import { useCallback, useEffect, useRef, useState } from "react"
import { Alert, FlatList, KeyboardTypeOptions, TouchableOpacity } from "react-native"
import * as yup from "yup"

const schema = yup.object().shape({
  cost: yup.number().required("Cost is required"),
  serviceDuration: yup.number().required("Duration is required"),
  name: yup.string().required("Service name is required"),
})

const ServiceDetailScreen = ({ route }) => {
  const data = route.params
  const detail = data.detail as ServiceDTO
  const [errors, setErrors] = useState({})
  const serviceRef = useRef<Partial<NewService>>({})
  const navigation = useNavigation<mainScreenProp>()

  const profileFields = [
    { id: "name", key: "name", label: "Service name", keyboardType: "default", value: detail.name },
    {
      id: "serviceDuration",
      key: "serviceDuration",
      label: "Duration",
      keyboardType: "numeric",
      value: detail.serviceDuration,
    },
    { id: "cost", key: "cost", label: "Cost", keyboardType: "numeric", value: detail.cost },
    { id: "color", key: "color", label: "Color", isOptional: true, keyboardType: "default" },
  ]
  const { addNewService, loadingNewService, errNewService, newService } = useService()

  useEffect(() => {
    if (newService) {
      navigation.goBack()
    }
  }, [newService])

  useEffect(() => {
    return () => {
      if (serviceRef && serviceRef.current) {
        serviceRef.current = null
      }
    }
  }, [])

  useEffect(() => {
    if (errNewService) {
      alert(errNewService.message)
    }
  }, [errNewService])

  const handleProfileChange = (id: string, value: string) => {
    switch (id) {
      case "name":
        serviceRef.current.name = value
        break
      case "serviceDuration":
        serviceRef.current.serviceDuration = parseInt(value)
        break
      case "cost":
        serviceRef.current.cost = parseInt(value)
        serviceRef.current.price = parseInt(value)
        break
      default:
        break
    }
  }

  const handleButtonPress = async () => {
    if (serviceRef.current) {
      try {
        const result = await schema.validate(serviceRef.current, { abortEarly: false })
        if (result) {
          setErrors({})
          addNewService(serviceRef.current)
        }
      } catch (error) {
        setErrors(convertYupErrorInner(error.inner))
      }
    }
  }

  const renderFooterComponent = useCallback(() => {
    return (
      <ButtonCustom
        disabled={loadingNewService}
        isLoading={loadingNewService}
        w="90%"
        marginBottom={spacing[2]}
        onPress={handleButtonPress}
      >
        <Text text={"screenStatus"} style={{ color: color.palette.white }} />
      </ButtonCustom>
    )
  }, [loadingNewService])

  const renderHeaderComponent = useCallback(
    () => (
      <TouchableOpacity>
        <Avatar
          source={{ uri: detail?.photo }}
          size={"2xl"}
          alignSelf="center"
          marginTop={spacing[1]}
        />
      </TouchableOpacity>
    ),
    [],
  )

  return (
    <Screen>
      <Header
        headerText={detail.name}
        leftIcon="back"
        onLeftPress={() => Alert.alert("left nav")}
      />
      <FlatList
        data={profileFields}
        showsVerticalScrollIndicator={false}
        ListHeaderComponent={renderHeaderComponent}
        keyExtractor={(item) => item.id}
        renderItem={({ item }) => {
          return (
            <TextFieldCustom
              {...item}
              keyboardType={item.keyboardType as KeyboardTypeOptions}
              style={{ marginHorizontal: spacing[4] }}
              opacity={1}
              paddingLeft={"1.5"}
              alignSelf="center"
              rounded={"md"}
              key={item.id}
              label={item.label}
              value={item.value ? item.value.toString() : ""}
              editable={true}
              onChangeText={(value) => handleProfileChange(item.id, value)}
              errorMsg={errors[item.id]}
              isDisabled={true}
            />
          )
        }}
      />
      {/* {renderFooterComponent()} */}
    </Screen>
  )
}

export default ServiceDetailScreen
