import { mainScreenProp } from "@navigators/main"
import { useFocusEffect, useNavigation } from "@react-navigation/native"
import { Fab } from "native-base"
import * as React from "react"
import { useCallback } from "react"
import { Alert, FlatList, Image, TouchableOpacity, View } from "react-native"
import { Header } from "@components/header/header"
import Loading from "@components/loading/Loading"
import { Screen } from "@components/screen/screen"
import SearchBar from "@components/search-bar"
import { RefSearch } from "@components/search-bar/SearchBar"
import Text from "@components/text"
import { useService } from "@hooks/service/useService"
import { ServiceDTO } from "@models/backend/response/Service"
import { color } from "@theme/color"
import { isNull } from "lodash"
import styles from "./styles"
import { ButtonCustom } from "@components/index"
import { spacing } from "@theme/spacing"

const Item = ({ item }: { item: ServiceDTO }) => (
  <View style={styles.item}>
    <View style={styles.containerItem}>
      <Image
        style={styles.serviceImage}
        source={{ uri: !isNull(item.photo) ? item.photo : "https://picsum.photos/200/300" }}
      />
    </View>
    <View style={styles.subContainerItem}>
      <View style={styles.viewNamePrice}>
        <Text ellipsizeMode="tail" numberOfLines={1} style={styles.serviceName}>
          {item.name}
        </Text>
        <Text style={styles.servicePrice}>{item.price + " $"}</Text>
      </View>

      <Text style={styles.serviceTime}>{item.serviceDuration + " minutes"}</Text>
    </View>
  </View>
)

const ServiceListScreen = () => {
  const ref = React.useRef<RefSearch>(null)
  const navigation = useNavigation<mainScreenProp>()

  const { loadingServiceList, getServiceList, serviceList } = useService()

  useFocusEffect(
    useCallback(() => {
      getServiceList("")
    }, []),
  )

  const cancelAction = () => {
    getServiceList("")
  }

  const searchAction = () => {
    if (ref && ref.current) {
      getServiceList(ref.current.searchPhrase.trim())
    }
  }

  const goDetail = (detail: ServiceDTO) => {
    navigation.navigate("serviceDetail", { detail })
  }

  const renderItem = ({ item }: { item: ServiceDTO }) => {
    return (
      <TouchableOpacity onPress={() => goDetail(item)}>
        <Item item={item} />
      </TouchableOpacity>
    )
  }

  const renderEmpty = useCallback(() => {
    return (
      <View style={styles.empty}>
        <Text tx="common.empty" />
      </View>
    )
  }, [])

  const renderServiceList = useCallback(() => {
    return (
      <View style={styles.viewServiceList}>
        {loadingServiceList ? (
          <Loading color={"black"} />
        ) : serviceList.length === 0 ? (
          renderEmpty()
        ) : (
          <FlatList
            data={serviceList}
            renderItem={renderItem}
            keyExtractor={(item) => item.id.toString()}
            showsVerticalScrollIndicator={false}
          />
        )}
      </View>
    )
  }, [serviceList, loadingServiceList])

  const renderNewService = useCallback(() => {
    return (
      <Fab
        position={"absolute"}
        bottom={50}
        onPress={() => navigation.navigate("newService")}
        _pressed={{ opacity: 0.2 }}
        h={16}
        w={16}
        label={"+"}
        size="lg"
        rounded={"full"}
        backgroundColor={color.palette.black}
        renderInPortal={false}
      />
    )
  }, [])

  const renderButtonCategory = React.useCallback(() => {
    return (
      <ButtonCustom
        isLoading={false}
        disabled={false}
        w="90%"
        marginTop={spacing[1]}
        onPress={handleCategoryButtonPress}
      >
        <Text text={"Category"} style={{ color: color.palette.white }} />
      </ButtonCustom>
    )
  }, [])
  const handleCategoryButtonPress = async () => {
    navigation.navigate("categoryList")
  }

  return (
    <Screen style={styles.body}>
      <Header
        headerTx="service.services"
        // leftIcon="back"
        onLeftPress={() => Alert.alert("left nav")}
      />
      <SearchBar searchAction={searchAction} cancelAction={cancelAction} ref={ref} />
      {renderButtonCategory()}
      {renderServiceList()}
      {renderNewService()}
    </Screen>
  )
}

export default ServiceListScreen
