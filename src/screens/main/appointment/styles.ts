import { color } from "@theme/color"
import { spacing } from "@theme/spacing"
import { StyleSheet } from "react-native"

const styles = StyleSheet.create({
  body: {
    flex: 1,
    padding: spacing[4],
  },
  childBetViewTime: {
    flex: 0.1,
  },
  childViewTime: {
    flex: 1,
  },
  footer: {
    backgroundColor: color.palette.black,
    flex: 1,
  },
  lbl: {
    color: color.palette.black,
    fontSize: 13,
    fontWeight: "500",
  },
  txtDate: {
    color: color.palette.black,
    fontSize: 15,
    fontWeight: "bold",
  },
  viewCustomer: {
    paddingTop: spacing[4],
  },
  viewDate: {
    alignItems: "center",
    backgroundColor: color.palette.lighterGrey,
    borderColor: color.palette.lighterGrey,
    borderWidth: 0.5,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: spacing[2],

    paddingVertical: spacing[4],
  },
  viewService: {
    paddingVertical: spacing[4],
  },
  viewTime: {
    alignItems: "center",
    flexDirection: "row",
    paddingVertical: spacing[4],
  },
})

export default styles
