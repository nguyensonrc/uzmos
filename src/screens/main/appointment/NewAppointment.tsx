import { ButtonCustom } from "@components/button/buttonCustom"
import { CustomCalendarList } from "@components/calendar/CustomCalendarList"
import { Header } from "@components/header/header"
import CustomModal, { IRefCustomModal } from "@components/modal/CustomModal"
import { Screen } from "@components/screen/screen"
import Text from "@components/text"
import { CALENDAR_FORMAT, DURATION, SELECT_HEIGHT } from "@config/constants"
import { useAppointment } from "@hooks/appointment/useAppointment"
import { useCustomer } from "@hooks/customer"
import { useService } from "@hooks/service/useService"
import { useStaff } from "@hooks/staff"
import { AppointmentDTO } from "@models/backend/response/Appointment"
import { mainScreenProp } from "@navigators/main"
import { useNavigation } from "@react-navigation/native"
import { color } from "@theme/color"
import { spacing } from "@theme/spacing"
import moment from "moment"
import {
  Box,
  ChevronDownIcon,
  FormControl,
  Select,
  TextArea,
  useToast,
  WarningOutlineIcon,
} from "native-base"
import * as React from "react"
import { useCallback, useEffect, useRef, useState } from "react"
import { TouchableOpacity, View } from "react-native"
import { DateData } from "react-native-calendars"
import * as yup from "yup"
import styles from "./styles"

const profileFields = [
  { id: "name", key: "name", label: "Service name", keyboardType: "default" },
  { id: "serviceDuration", key: "serviceDuration", label: "Duration", keyboardType: "numeric" },
  { id: "cost", key: "cost", label: "Cost", keyboardType: "numeric" },
  { id: "color", key: "color", label: "Color", isOptional: true, keyboardType: "default" },
]

const schema = yup.object().shape({
  cost: yup.number().required("Cost is required"),
  serviceDuration: yup.number().required("Duration is required"),
  name: yup.string().required("Service name is required"),
})

const TIME_SLOTS_CONFIG = {
  nextSlot: 15,
  breakTime: [["12:00", "13:00"]],
  startTime: "08:00",
  endTime: "20:00",
}

const isInBreak = (slotTime: moment.Moment, breakTimes: string[][]) => {
  try {
    return breakTimes.some((br) => {
      return slotTime >= moment(br[0], "HH:mm") && slotTime < moment(br[1], "HH:mm")
    })
  } catch (error) {
    return false
  }
}

const NewAppointmentScreen = () => {
  const [customer, setCustomer] = useState("")
  const [service, setService] = useState("")
  const [staff, setStaff] = useState("")
  const [startTime, setStartTime] = useState("")
  const [duration, setDuration] = useState("")
  const [notes, setNotes] = useState("")
  const [currentDate, setCurrentDate] = useState(moment())
  const [timeSlot, setTimeSlot] = useState([])
  const navigation = useNavigation<mainScreenProp>()
  const modalRef = useRef<IRefCustomModal>(null)
  const toast = useToast()

  const { addNewAppointment, loadingNewAppointment, newAppointmnet, errNewAppoint } =
    useAppointment()
  const { getServiceList, serviceList } = useService()
  const { getStaff, staffs } = useStaff()
  const { getCustomers, customers, skip } = useCustomer()
  const endTime = moment(TIME_SLOTS_CONFIG.endTime, "HH:mm")

  useEffect(() => {
    getCustomers(skip, "")
    getServiceList("")
    getStaff()
  }, [])

  useEffect(() => {
    timeSelector()
  }, [currentDate])

  useEffect(() => {
    if (newAppointmnet) {
      toast.show({
        render: () => {
          return (
            <Box bg="emerald.500" px="2" py="1" rounded="sm" mb={5}>
              Appointment Created!
            </Box>
          )
        },
      })
      navigation.goBack()
    }
  }, [newAppointmnet])

  useEffect(() => {
    if (errNewAppoint && errNewAppoint.message) {
      toast.show({
        render: () => {
          return (
            <Box bg="emerald.500" px="2" py="1" rounded="sm" mb={5}>
              {errNewAppoint.message}
            </Box>
          )
        },
      })
    }
  }, [errNewAppoint])

  const checkCurrentDate = () => {
    try {
      if (currentDate.isSame(moment(), "days")) {
        return 0
      } else if (currentDate.isAfter(moment(), "days")) {
        return 1
      }
      return -1
    } catch (error) {
      __DEV__ && console.log({ error })
      return -1
    }
  }

  const validateData = () => {
    if (
      checkCurrentDate() > -1 &&
      service.length > 0 &&
      startTime.length > 0 &&
      duration.length > 0
    ) {
      return true
    } else {
      return false
    }
  }

  const timeSelector = () => {
    try {
      const times = []
      let slotTime = moment()
      const checkDate = checkCurrentDate()
      if (checkDate === -1) {
        // pass date
        if (timeSlot.length > 0) {
          setTimeSlot([])
        }
        return
      } else if (checkDate === 0) {
        // current date
        const currentMin = moment().minutes()
        let minOffet = "00"
        let hourOffet = moment().hours()
        if (currentMin === 0 || currentMin < 15) {
          minOffet = "15"
        } else if (currentMin === 15 || currentMin < 30) {
          minOffet = "30"
        } else if (currentMin === 30 || currentMin < 45) {
          minOffet = "45"
        } else if (currentMin === 45 || currentMin < 60) {
          minOffet = "00"
          hourOffet = hourOffet + 1
        }

        slotTime = moment(hourOffet + ":" + minOffet, "HH:mm")
      } else if (checkCurrentDate() === 1) {
        // future date
        slotTime = moment(TIME_SLOTS_CONFIG.startTime, "HH:mm")
      }
      while (slotTime < endTime) {
        if (!isInBreak(slotTime, TIME_SLOTS_CONFIG.breakTime)) {
          times.push(slotTime.format("HH:mm"))
        }
        slotTime = slotTime.add(TIME_SLOTS_CONFIG.nextSlot, "minutes")
      }
      setTimeSlot(times)
    } catch (error) {
      __DEV__ && console.log({ error })
    }
  }

  const submit = () => {
    if (validateData()) {
      const data = {} as AppointmentDTO
      let startHour = 0
      let startMin = 0
      if (notes.length > 0) {
        data.note = notes
      }
      if (customer.length > 0) {
        data.customerId = parseInt(customer.trim())
      }
      if (service.length > 0) {
        data.serviceId = parseInt(service.trim())
      }
      if (staff.length > 0) {
        data.staffId = parseInt(staff.trim())
      }
      if (startTime.length > 0) {
        const arr = startTime.split(":")
        startHour = parseInt(arr[0])
        startMin = parseInt(arr[1])
      }

      const convertDateUTC = moment(currentDate.set({ h: startHour, m: startMin })).utc()

      data.date = convertDateUTC.toDate()
      if (duration.length > 0) {
        const convertLastDateUTC = moment(
          currentDate.set({ h: startHour, m: startMin + parseInt(duration.trim()) }),
        ).utc()
        data.lastDate = convertLastDateUTC.toDate()
        data.duration = parseInt(duration.trim())
      }
      // console.log(data.date)
      // console.log(data.lastDate)
      // console.log(moment().utc().format("YYYY-MM-DD hh:mm:ss"))
      // console.log({ data })
      addNewAppointment(data)
    }
  }

  const openCalendar = () => {
    if (modalRef && modalRef.current) {
      modalRef.current.openModal()
    }
  }

  const selectService = (serviceId: string) => {
    setService(serviceId)
    if (serviceId.length > 0) {
      const temp = serviceList.find((element) => element.id === parseInt(serviceId.trim()))
      if (temp && temp.serviceDuration) {
        const durationTemp = DURATION.find((element) => element.value === temp.serviceDuration)
        if (durationTemp && durationTemp.value) {
          setDuration(durationTemp.value.toString())
        }
      }
    }
  }

  const selectedDate = (date: DateData) => {
    try {
      if (modalRef && modalRef.current) {
        modalRef.current.closeModal()
      }
      if (date && date.timestamp) {
        setDuration("")
        setStartTime("")
        setCurrentDate(moment(date.timestamp))
      }
    } catch (error) {
      __DEV__ && console.log({ error })
    }
  }

  const RenderHeader = useCallback(
    () => <Header headerTx="appointment.newAppointment" leftIcon="back" />,
    [],
  )

  const RenderBody = useCallback(() => {
    return (
      <View style={styles.body}>
        {/* date */}
        <TouchableOpacity onPress={openCalendar}>
          <View style={styles.viewDate}>
            <Text style={styles.txtDate}>{currentDate.format(CALENDAR_FORMAT)}</Text>
            <Box>
              <ChevronDownIcon size="5" />
            </Box>
          </View>
        </TouchableOpacity>
        {/* customer */}
        <FormControl style={styles.viewCustomer} isRequired isInvalid={false}>
          <Text tx="appointment.customer" style={styles.lbl} />
          <Select
            selectedValue={customer}
            height={SELECT_HEIGHT}
            accessibilityLabel="Add a Customer"
            placeholder="Add a Customer"
            _selectedItem={{
              bg: "teal.600",
              endIcon: <ChevronDownIcon size="5" />,
            }}
            mt={1}
            onValueChange={(itemValue) => setCustomer(itemValue)}
          >
            {customers && customers.length > 0
              ? customers.map((element) => {
                  return (
                    <Select.Item
                      key={element.id}
                      label={element.firstName + element.lastName}
                      value={element.id.toString()}
                    />
                  )
                })
              : null}
          </Select>
          <FormControl.ErrorMessage leftIcon={<WarningOutlineIcon size="xs" />}>
            Please make a selection!
          </FormControl.ErrorMessage>
        </FormControl>
        {/* service */}
        <FormControl style={styles.viewService} isRequired isInvalid={service.length === 0}>
          <Text tx="service.service" style={styles.lbl} />
          <Select
            selectedValue={service}
            height={SELECT_HEIGHT}
            accessibilityLabel="Add a Service"
            placeholder="Add a Service"
            _selectedItem={{
              bg: "teal.600",
              endIcon: <ChevronDownIcon size="5" />,
            }}
            mt={1}
            onValueChange={selectService}
          >
            {serviceList && serviceList.length > 0
              ? serviceList.map((element) => {
                  return (
                    <Select.Item
                      key={element.id}
                      label={element.name}
                      value={element.id.toString()}
                    />
                  )
                })
              : null}
          </Select>
          <FormControl.ErrorMessage leftIcon={<WarningOutlineIcon size="xs" />}>
            Please make a selection!
          </FormControl.ErrorMessage>
        </FormControl>
        {/* staff */}
        <FormControl isRequired isInvalid={false}>
          <Text tx="appointment.staff" style={styles.lbl} />
          <Select
            selectedValue={staff}
            height={SELECT_HEIGHT}
            accessibilityLabel=""
            placeholder=""
            _selectedItem={{
              bg: "teal.600",
              endIcon: <ChevronDownIcon size="5" />,
            }}
            mt={1}
            onValueChange={(itemValue) => setStaff(itemValue)}
          >
            {staffs && staffs.length > 0
              ? staffs.map((element) => {
                  return (
                    <Select.Item
                      key={element.id}
                      label={element.name}
                      value={element.id.toString()}
                    />
                  )
                })
              : null}
          </Select>
          <FormControl.ErrorMessage leftIcon={<WarningOutlineIcon size="xs" />}>
            Please make a selection!
          </FormControl.ErrorMessage>
        </FormControl>
        {/* time */}
        <View style={styles.viewTime}>
          <View style={styles.childViewTime}>
            <FormControl isRequired isInvalid={startTime.length === 0}>
              <Text tx="appointment.startTime" style={styles.lbl} />
              <Select
                selectedValue={startTime}
                height={SELECT_HEIGHT}
                accessibilityLabel=""
                placeholder=""
                _selectedItem={{
                  bg: "teal.600",
                  endIcon: <ChevronDownIcon size="5" />,
                }}
                mt={1}
                onValueChange={(itemValue) => setStartTime(itemValue)}
              >
                {timeSlot && timeSlot.length > 0
                  ? timeSlot.map((element) => {
                      return <Select.Item key={element} label={element} value={element} />
                    })
                  : null}
              </Select>
              <FormControl.ErrorMessage leftIcon={<WarningOutlineIcon size="xs" />}>
                Please make a selection!
              </FormControl.ErrorMessage>
            </FormControl>
          </View>
          <View style={styles.childBetViewTime} />
          <View style={styles.childViewTime}>
            <FormControl isRequired isInvalid={duration.length === 0}>
              <Text tx="appointment.duration" style={styles.lbl} />
              <Select
                selectedValue={duration}
                height={SELECT_HEIGHT}
                accessibilityLabel=""
                placeholder=""
                _selectedItem={{
                  bg: "teal.600",
                  endIcon: <ChevronDownIcon size="5" />,
                }}
                mt={1}
                onValueChange={(itemValue) => setDuration(itemValue)}
              >
                {DURATION.map((element) => {
                  return (
                    <Select.Item
                      key={element.value}
                      label={element.label}
                      value={element.value.toString()}
                    />
                  )
                })}
              </Select>
              <FormControl.ErrorMessage leftIcon={<WarningOutlineIcon size="xs" />}>
                Please make a selection!
              </FormControl.ErrorMessage>
            </FormControl>
          </View>
        </View>
        {/* notes */}
        <Text tx="appointment.notes" style={styles.lbl} />
        <TextArea
          // value={notes}
          onChangeText={(text) => setNotes(text)}
          h={20}
          placeholder="Notes visible to staff only"
          w="100%"
          autoCompleteType={"off"}
        />
        {/* calendar picker modal */}
        <CustomModal
          ref={modalRef}
          childView={<CustomCalendarList currentDate={currentDate} selectedDate={selectedDate} />}
        />
      </View>
    )
  }, [serviceList, staffs, customers, customer, service, staff, startTime, duration])

  const RenderFooter = () => {
    return (
      <ButtonCustom
        disabled={
          loadingNewAppointment ||
          service.length === 0 ||
          duration.length === 0 ||
          startTime.length === 0
        }
        isLoading={loadingNewAppointment}
        w="90%"
        h={SELECT_HEIGHT}
        marginBottom={spacing[2]}
        onPress={submit}
      >
        <Text tx="common.save" style={{ color: color.palette.white }} />
      </ButtonCustom>
    )
  }

  return (
    <Screen>
      <RenderHeader />
      <RenderBody />
      <RenderFooter />
    </Screen>
  )
}

export default NewAppointmentScreen
