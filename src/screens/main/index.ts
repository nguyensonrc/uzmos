import NewAppointmentScreen from "./appointment/NewAppointment"
import { CustomerListScreen } from "./customer"
import HomeScreen from "./home"
import CategoryDetailScreen from "./services/category/CategoryDetail"
import NewCategoryScreen from "./services/category/NewCategory"
import CategoryListScreen from "./services/categoryList"
import NewServiceScreen from "./services/service"
import ServiceDetailScreen from "./services/service/ServiceDetail"
import ServiceListScreen from "./services/serviceList"
import CompanyHoursScreen from "./settings/CompanyHours/CompanyHours"
import TimeZonesScreen from "./settings/CompanyHours/TimeZones"
import SettingsScreen from "./settings/Settings"
import BreaksScreen from "./staff/Breaks"
import TimeOffScreen from "./staff/TimeOff"
import TimeOffDetailScreen from "./staff/TimeOffDetail"
import WorkingDaysScreen from "./staff/WorkingDays"

export {
  HomeScreen,
  CustomerListScreen,
  ServiceListScreen,
  NewServiceScreen,
  CategoryListScreen,
  ServiceDetailScreen,
  CategoryDetailScreen,
  NewAppointmentScreen,
  NewCategoryScreen,
  WorkingDaysScreen,
  BreaksScreen,
  TimeOffScreen,
  TimeOffDetailScreen,
  SettingsScreen,
  CompanyHoursScreen,
  TimeZonesScreen,
}
