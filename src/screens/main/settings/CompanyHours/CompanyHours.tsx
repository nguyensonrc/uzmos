import { ButtonCustom } from "@components/button/buttonCustom"
import { Header } from "@components/header/header"
import { Screen } from "@components/index"
import Text from "@components/text/text"
import VectorIcon from "@components/vectorIcon/vectorIcon"
import { SELECT_HEIGHT } from "@config/constants"
import { useStoresInfo } from "@hooks/settings/useStoresInfo"
import { OpenHoursDTO, StoreDTO, TimeZoneDTO } from "@models/backend/response/Store"
import { mainScreenProp } from "@navigators/main"
import { useNavigation } from "@react-navigation/native"
import { color } from "@theme/color"
import { spacing } from "@theme/spacing"
import _ from "lodash"

import { View } from "native-base"
import React, { useCallback, useEffect, useState } from "react"
import { Switch, TouchableOpacity } from "react-native"
import styles from "./styles"
const moment = require("moment-timezone")
const CompanyHoursScreen = ({ route }) => {
  const { storeDetail, timeZone } = route.params
  const store = storeDetail as StoreDTO
  const timeZoneTemp = timeZone as TimeZoneDTO
  const currentTimeZone = store.timezone ? store.timezone: moment.tz.guess();
  
  const offSet = timeZone ? parseInt(timeZoneTemp.rawOffsetInMinutes) / 60 : moment.tz(currentTimeZone).format('Z')
  const currentTime = timeZone ? timeZoneTemp.name : currentTimeZone
  const navigation = useNavigation<mainScreenProp>()

  const [data, setData] = useState<OpenHoursDTO[]>(store.openHours)

  const { loadingCompanyHour, companyHour, updateCompanyHour } = useStoresInfo()

  const navTimeZone = () => {
    navigation.navigate("timeZone", { storeDetail, timeZone: timeZone ? timeZoneTemp : { "group": [currentTimeZone] } as any })
  }

  const toggleSwitch = (day: number, prev: boolean) => {
    setData(
      [...data].map((object) => {
        if (object.day === day) {
          return {
            ...object,
            open: !prev,
          }
        } else return object
      }),
    )
  }

  const convertDay = (day: number) => {
    switch (day) {
      case 0:
        return "Sunday"
      case 1:
        return "Monday"
      case 2:
        return "TuesDay"
      case 3:
        return "Wednesday"
      case 4:
        return "Thursday"
      case 5:
        return "Friday"
      case 6:
        return "Saturday"
      default:
        return "Monday"
    }
  }

  const save = () => {
    try {
      const dif = _.differenceWith(data, store.openHours, _.isEqual)
      // console.log({ dif })
      if ((dif && dif.length > 0) || currentTimeZone !== currentTime) {
        store.openHours = data
        store.timezone = timeZone ? timeZoneTemp.name : currentTimeZone
        updateCompanyHour(store.id, store)
      } else {
        navigation.goBack()
      }
    } catch (error) {
      __DEV__ && console.log({ error })
    }
  }

  useEffect(() => {
    if (companyHour) {
      alert("Update Company Hours successful!")
      navigation.goBack()
      //   navigation.navigate({
      //     name: "staffProfile",
      //     params: { detail: staffData, editable: false },
      //     merge: true,
      //   })
    }
  }, [companyHour])

  const RenderItem = ({ item }: { item: OpenHoursDTO }) => {
    return (
      <View key={item.id} style={styles.itemWorkingDays}>
        <View>
          <Text style={styles.lblitemWorkingDays}>{convertDay(item.day)}</Text>
          <Text style={styles.lblitemTimeWorkingDays}>{item.fromHour + " -> " + item.toHour}</Text>
        </View>
        <Switch
          trackColor={{ false: "#767577", true: "#81b0ff" }}
          thumbColor={"#fff"}
          ios_backgroundColor="#fff"
          onValueChange={() => toggleSwitch(item.day, item.open)}
          value={item.open}
        />
      </View>
    )
  }
  const RenderHeader = useCallback(
    () => <Header headerText={"Company Hours"} leftIcon="back" />,
    [],
  )
  const RenderFooter = () => {
    return (
      <ButtonCustom
        disabled={loadingCompanyHour}
        isLoading={loadingCompanyHour}
        w="90%"
        h={SELECT_HEIGHT}
        marginBottom={spacing[2]}
        onPress={save}
      >
        <Text tx="common.save" style={{ color: color.palette.white }} />
      </ButtonCustom>
    )
  }

  const RenderTimeZone = () => {


    return (
      <TouchableOpacity onPress={navTimeZone}>
        <View style={styles.viewContainTimeOff}>
          <View style={styles.viewParentCenterTimeOff}>
            <Text style={styles.txtDate}>{"Central Standard Time"}</Text>
            <Text style={styles.lblitemWorkingDays}>{"(UTC" + offSet + ") Time (" + currentTime + ")"}</Text>

          </View>
          <View style={styles.viewLstRightTimeOff}>
            <VectorIcon iconSet="ion" name="chevron-forward" />
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  return (
    <Screen>
      <RenderHeader />
      <RenderTimeZone />
      <View style={styles.childViewTime}>
        {data.map((element: OpenHoursDTO) => {
          return <RenderItem key={element.id} item={element} />
        })}
      </View>
      <RenderFooter />
    </Screen>
  )
}

export default CompanyHoursScreen
