import { Header } from "@components/header/header"
import { Screen } from "@components/index"
import Loading from "@components/loading/Loading"
import Text from "@components/text"
import { useStoresInfo } from "@hooks/settings/useStoresInfo"
import { mainScreenProp } from "@navigators/main"
import { navigate } from "@navigators/navigation-utilities"
import { useFocusEffect, useNavigation } from "@react-navigation/native"
import * as React from "react"
import { useCallback } from "react"
import { TouchableOpacity, View } from "react-native"
import styles from "./styles"

const SettingsScreen = () => {
  const { loadingStores, stores, getStores } = useStoresInfo()
  const navigation = useNavigation<mainScreenProp>()

  useFocusEffect(
    useCallback(() => {
      getStores()
    }, []),
  )

  const navCompanyDetail = () => {
    console.log("")
    navigate("storeDetail", { storeDetail: stores[0] })
  }

  const navCompanyHours = () => {
    if (stores && stores.length > 0 && stores[0].openHours) {
      navigation.navigate("companyHours", { storeDetail: stores[0] })
    }
  }

  const navBookingPolicies = () => {
    console.log("")
    navigate("bookingPolicies", { storeDetail: stores[0] })
  }

  const RenderHeader = useCallback(() => <Header headerText={"Settings"} />, [])

  const renderEmpty = useCallback(() => {
    return (
      <View style={styles.empty}>
        <Text tx="common.empty" />
      </View>
    )
  }, [])
  console.log("alo1", stores)
  return (
    <Screen>
      <RenderHeader />
      {loadingStores ? (
        <Loading color={"black"} />
      ) : stores.length === 0 ? (
        renderEmpty()
      ) : (
        <>
          <TouchableOpacity onPress={navCompanyDetail}>
            <Text>Company Detail</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={navCompanyHours}>
            <Text>Company Hours</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={navBookingPolicies}>
            <Text>Booking Policies</Text>
          </TouchableOpacity>
        </>
      )}
    </Screen>
  )
}

export default SettingsScreen
